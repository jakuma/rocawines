var $ = jQuery;
$( document ).on( "ready", function( ) {
    var page = $( "#page" ).val( );
    if ( page == 'index' ) {
        setTimeout( function( ) {
            $( "#inicioLoading" ).hide( );
            $( ".navbar-roca" ).delay( 800 ).fadeIn( "slow" );
        }, 1500 )
    } else {
        //SLOW SHOW MENU
        $( ".navbar-roca" ).delay( 1800 ).fadeIn( "slow" );
    }

    $( ".fadeIn" ).hide( ).delay( 1200 ).fadeIn( "slow" )

    $( "#enviado" ).hide( ).html( "" )
    var base_url = $( "#base_url" ).val( );

    $( document ).on( "click", ".enviaremails", enviarmails )

    function enviarmails( ) {
        $( this ).html( "<i class='fa fa-spinner'></i> Enviando ..." )
        $( "#enviado" ).hide( ).html( "" )
        var nombre = $( "#nombre" ).val( ),
            email = $( "#email" ).val( ),
            edad = $( "#edad" ).val( ),
            telefono = $( "#telefono" ).val( ),
            asunto = 'Me Quiero Inscribir',
            provincia = $( "#provincia" ).val( ) || '',
            ciudad = $( "#ciudad" ).val( ) || '',
            conocistes = $( "#iscc_Conocer" ).val( ) || '',
            interesa = $( "#selCat" ).val( ) || '',
            dedonde = $( "#dedonde" ).val( ) || 'web',
            curso = $( 'curso' ).val( ) || '',
            tipo = 'interesa'

        url = base_url + "/enviarmail?nombre=" + nombre + "&email=" + email + "&telefono=" + telefono + "&asunto=" + asunto;
        url += "&provincia=" + provincia + "&ciudad=" + ciudad + '&conocistes=' + conocistes + '&interesa=' + interesa + '&tipo=' + tipo + '&curso=' + curso + '&dedonde=' + dedonde
        console.log( url )
        $.ajax( {
            url: url,
            method: "GET",
            success: function( data ) {
                $( '.enviaremails' ).html( '<i class="fa fa-check"></i> Enviado' )
                $( "#enviado" ).html( "<i class='fa fa-check'></>Mensaje Enviado Gracias!" ).show( )
            }
        } )

    }



    $( document ).on( 'scroll', function( ) {

        if ( $( window ).scrollTop( ) > 150 ) {
            if ( !$( ".navbar-roca" ).is( "navScroll" ) ) {
                $( ".navbar-roca" ).addClass( "navScroll" )
            }
            if ( !$( ".brand-centered" ).is( "smaller" ) ) {
                $( ".brand-centered" ).addClass( "smaller" )
            }
            //$(".brand-centered ").toggleClass("smaller")
        } else {

            $( ".navbar-roca" ).removeClass( "navScroll" )
            $( ".brand-centered" ).removeClass( "smaller" )

        }
    } )

    $( ".modalinfo" ).on( "click", function( ) {
        var id = $( this ).attr( "rel" )
        var altura = $( window ).height( ) - 150
        $( "#" + id ).modal( "show" ).find( ".modal-dialog" ).css( {
            top: "50px"
        } )
    } )

    function sizeModal( el ) {
        var heigthModal = $( el ).find( '.modal-content' ).height( )
        var winheigth = $( window ).height( )
        var marginTop = ( winheigth - heigthModal ) / 2
        return marginTop
    }

    //---------- RESIZE MODAL ---------------- //
    function setModalMaxHeight( element ) {
        this.$element = $( element );
        this.$content = this.$element.find( '.modal-content' );
        var borderWidth = this.$content.outerHeight( ) - this.$content.innerHeight( );
        var dialogMargin = $( window ).width( ) < 768 ? 20 : 60;
        var contentHeight = $( window ).height( ) - ( dialogMargin + borderWidth );
        var headerHeight = this.$element.find( '.modal-header' ).outerHeight( ) || 0;
        var footerHeight = this.$element.find( '.modal-footer' ).outerHeight( ) || 0;
        var maxHeight = contentHeight - ( headerHeight + footerHeight );

        this.$content.css( {
            'overflow': 'hidden'
        } );

        this.$element
            .find( '.modal-body' ).css( {
                'max-height': maxHeight,
                'overflow-y': 'auto'
            } );
    }

    $( '.modal' ).on( 'show.bs.modal', function( ) {
        $( this ).show( );
        setModalMaxHeight( this );
    } );

    $( window ).resize( function( ) {
        if ( $( '.modal.in' ).length != 0 ) {
            setModalMaxHeight( $( '.modal.in' ) );
        }
    } );

    $( "#menuleft" ).on( "click", function( ) {

        var visible = $( this ).hasClass( 'slide-active' );
        $( this ).toggleClass( 'slide-active', !visible );

        $( '#navRoca' ).stop( ).animate( {
            left: visible ? '-100%' : '0px'
        } );
    } )

    //Google Maps JS
    //Set Map
    function initialize( ) {
        var elcenter = new google.maps.LatLng( -34.635595, -68.277973 );
        var pointer = new google.maps.LatLng( -34.676310, -68.242480 );
        var imagePath = base_url + '/images/iconMaps.png'
        var mapOptions = {
            zoom: 12,
            center: elcenter,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [ {
                "featureType": "landscape.man_made",
                "elementType": "all",
                "stylers": [ {
                    "visibility": "off"
                } ]
            }, {
                "featureType": "landscape.natural",
                "elementType": "all",
                "stylers": [ {
                    "saturation": -100
                }, {
                    "gamma": 2
                } ]
            }, {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [ {
                    "saturation": -100
                }, {
                    "gamma": 1.22
                } ]
            }, {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [ {
                    "lightness": 1
                }, {
                    "hue": "#ff4400"
                }, {
                    "gamma": 0.68
                }, {
                    "saturation": -32
                } ]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [ {
                    "color": "#b8b8b8"
                }, {
                    "lightness": 18
                }, {
                    "visibility": "on"
                } ]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry.stroke",
                "stylers": [ {
                    "color": "#b6b6b6"
                } ]
            }, {
                "featureType": "road.local",
                "elementType": "all",
                "stylers": [ {
                    "visibility": "on"
                }, {
                    "color": "#656565"
                } ]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [ {
                    "color": "#cdcdcd"
                }, {
                    "lightness": 16
                } ]
            }, {
                "featureType": "road.local",
                "elementType": "geometry.stroke",
                "stylers": [ {
                    "color": "#b6b6b6"
                }, {
                    "visibility": "on"
                } ]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.stroke",
                "stylers": [ {
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                } ]
            }, {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [ {
                    "saturation": -100
                } ]
            }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [ {
                    "hue": "#003bff"
                }, {
                    "gamma": 1.45
                } ]
            } ]
        }

        var map = new google.maps.Map( document.getElementById( 'map' ), mapOptions );
        //Callout Content
        var contentString = '<div style="overflow:hidden;min-width:200px;"><h4 style="margin:0px;padding:0px;">BODEGA ROCA</h4><br><p>Ruta Provincial 165 esq. La Pichana.<br>M5603XAG  Cañada Seca - San Rafael <br> Mendoza - Argentina</p></div>';
        //Set window width + content
        var infowindow = new google.maps.InfoWindow( {
            content: contentString,
            maxWidth: 500
        } );

        //Add Marker
        var marker = new google.maps.Marker( {
            position: pointer,
            map: map,
            icon: imagePath,
            title: 'image title'
        } );

        google.maps.event.addListener( marker, 'click', function( ) {
            infowindow.open( map, marker );
        } );

        //Resize Function
        google.maps.event.addDomListener( window, "resize", function( ) {
            var center = map.getCenter( );
            google.maps.event.trigger( map, "resize" );
            map.setCenter( center );
        } );
    }

    google.maps.event.addDomListener( window, 'load', initialize );

    google.maps.event.addDomListener( window, 'load', initMapMundo );


    function initMapMundo( ) {
        var imagePath = base_url + '/images/iconMapsRoca.png'
        var delay = 130;
        var infowindow = new google.maps.InfoWindow( );
        var latlng = new google.maps.LatLng( 16, 3 );
        var mapOptions = {
            zoom: 2,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var geo = new google.maps.Geocoder( );
        var map = new google.maps.Map( document.getElementById( "mapM" ), mapOptions );
        var bounds = new google.maps.LatLngBounds( );



        var infowindow = new google.maps.InfoWindow( );
        var marker, i;
        var infoWindowContent = [ ];

        var addresses = [ {
            name: 'SOCIÉTÉ DES ALCOOLS DU QUEBEC',
            dir: '905, AVENUE DE LORIMIER, MONTREAL, QUEBEC; H2K 3V9 CANADA ',
            web: 'www.saq.com',
            pais: 'Canada'
        }, {
            name: 'NOVA SCOTIA LIQUOR CONTROL',
            dir: '93 CHAIN LAKE DRIVER, HALIFAX, NOVA SCOTIA',
            web: 'www.mynslc.com',
            pais: 'Canada'
        }, {
            name: 'LIQUOR CONTROL BOARD OF ONTARIO',
            dir: '55 LAKE SHORE BLVR. E TORONTO CANADA',
            web: 'www.lcbo.com',
            pais: 'Canada'

        }, {
            name: 'The Wine Co.',
            dir: 'Carr. Transpeninsular 39, Chulavista, San José del Cabo, B.C.S., México',
            web: 'www.thewineco.mx',
            pais: 'Mexico'

        }, {
            name: 'TYRON IMPORT EXPORT SAC',
            dir: 'Calle Los Milagros, Distrito de Lima, PERÚ',
            web: 'tyron.importexportsac@gmail.com',
            pais: 'Peru'
        }, {
            name: 'TREZORWINES Sp. Z.o.o.',
            dir: 'Ul. Loefflera 5, 25 – 550 Kielce, Polonia',
            web: 'tomasz@trezorwines.pl',
            pais: 'Polonia'
        }, {
            name: 'VICOM S.R.O.',
            dir: 'Jankovcova 6 1700 00 Praha 7 CZECH REPUBLIC',
            web: 'www.vicom-vino.cz',
            pais: 'Rep Checa'
        }, {
            name: 'HAND PICKED SELECTIONS',
            dir: '400 Holiday Ct, Unit 201, WARRENTON VIRGINIA VA 20186 USA',
            web: 'www.handpickedselections.com',
            pais: 'USA'
        }, {
            name: 'SPECIALTY CELLARS',
            dir: '13017 LA DANA COURT, SANTA FE SPRINGS, CA 90670 USA',
            web: 'www.specialtycellars.com',
            pais: 'USA'
        }, {
            name: 'Casa Flora Ltda.',
            dir: 'RUA BLUMENAU 695, SALA 7, BAIRRO SAO JOAO, ITAJAÍ,SANTA CATARINA',
            web: 'www.casaflora.com.br',
            pais: 'Brasil'
        }, {
            name: 'MODERNA ALIMENTOS S.A.',
            dir: 'SAN GABRIEL OE7 89 Y VALDERRAMA, QUITO',
            web: 'www.modernaalimentos.com.ec',
            pais: 'Ecuador'
        }, {
            name: 'SHANDONG QIAOYU INTERNATIONAL TRADE CO. LTD',
            dir: 'Room 401,Comprehensive Bonded Zone Customs Clearance Service Center in Dongying,Dongying City,Shandong',
            web: '405273616@qq.com',
            pais: 'China'
        }, {
            name: 'NECA SARL',
            dir: '5 RUE DE LPINOCHE – 67000 STRANSBOURG - FRANCE',
            web: 'www.vinsargentins.com',
            pais: 'Francia'
        }, {
            name: 'SMITH, RUSSELL & CO. LTD.',
            dir: '38 HAGLEY PARK ROAD KINGSTON 10, JAMAICA ',
            web: 'srl@cwjamaica.com',
            pais: 'Jamaica'
        }, {
            name: 'WUXI ZYDA INTERNATIONAL CO LTD',
            dir: '2 Xiangjiang Rd, 新区 Wuxi Shi, Jiangsu Sheng, China, 214028',
            web: 'zyda168@126.com',
            pais: 'China'

        }, {
            name: 'A & A WINES LTD.',
            dir: '13 Manfield Park, Cranleigh GU6 8PT, Reino Unido',
            web: 'www.aawines.co.uk',
            pais: 'Inglaterra'
        }, {
            name: 'SAN RAFAEL IMPORT',
            dir: 'WEZELKOOG 5, ALKMAAR, NETHERLANDS – 1822 BL',
            web: 'adeniet@rocanederland.nl',
            pais: 'Holanda'
        }, {
            name: 'TIANJIN INTERNATIONAL WEIDA TECHNOLOGY INNOVATION TRADE CO. LTD',
            dir: '2405-8 West side of Sunnyland Building Eleven. Hedong, District of Tianjin ',
            web: 'tianjinweida2013@163.com ',
            pais: 'China'
        } ];
      
            // ====== Geocoding ======
        function getAddress( search, next ) {
            geo.geocode( {
                address: search.dir
            }, function( results, status ) {
                // If that was successful
                if ( status == google.maps.GeocoderStatus.OK ) {
                    // Lets assume that the first marker is the one we want
                    var p = results[ 0 ].geometry.location;
                    var lat = p.lat( );
                    var lng = p.lng( );
                    // Output the data
                    var msg = 'address="' + search.name + '" lat=' + lat + ' lng=' + lng + '(delay=' + delay + 'ms)<br>';
                    console.log( msg )
                        // Create a marker
                    createMarker( search, lat, lng );
                }
                // ====== Decode the error status ======
                else {
                    // === if we were sending the requests to fast, try this one again and increase the delay
                    if ( status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT ) {
                        console.log( status + ' - nombre:' + search.name )
                        nextAddress--;
                        delay++;
                    } else {
                        var reason = "Code " + status;
                        var msg = 'address="' + search.dir + '" error=' + reason + '(delay=' + delay + 'ms)<br>';
                        // document.getElementById( "messages" ).innerHTML += msg;

                    }
                }
                next( );
            } );
        }

        // ======= Function to create a marker
        function createMarker( add, lat, lng ) {
            var contentString = '<div style="overflow:hidden;min-width:200px"><h4 style="margin:0px;padding:0px;">';
            contentString += add.name
            contentString += ' <small>( '
            contentString += add.pais
            contentString += ' )</small></h4><br><p>'

            if ( add.web.indexOf( '@' ) < 0 ) {

                contentString += '<a href="http://' + add.web + '" target="_blank">'
            } else {

                contentString += '<a href="mailto:' + add.web + '">'
            }
            contentString += add.web + '</a></p></div>'
            var marker = new google.maps.Marker( {
                position: new google.maps.LatLng( lat, lng ),
                map: map,
                icon: imagePath,
                zIndex: Math.round( latlng.lat( ) * -100000 ) << 5
            } );

            google.maps.event.addListener( marker, 'click', function( ) {
                infowindow.setContent( contentString );
                infowindow.open( map, marker );
            } );

            bounds.extend( marker.position );

        }

        // ======= Global variable to remind us what to do next
        var nextAddress = 0;

        // ======= Function to call the next Geocode operation when the reply comes back

        function theNext( ) {

            if ( nextAddress <= 7 ) {

                setTimeout( getAddress( addresses[ nextAddress ], theNext ), delay );
                nextAddress++;
            } else if ( nextAddress < addresses.length ) {
                setTimeout( function( ) {


                    setTimeout( getAddress( addresses[ nextAddress ], theNext ), delay );
                    nextAddress++;
                }, 300 )

            } else {


                // We're done. Show map bounds
                map.fitBounds( bounds );
            }
        }

        // ======= Call that function for the first time =======
        theNext( );
        // function geocodeAddress( locationD, next ) {

        //     geocoder.geocode( {

        //         address: locationD.dir

        //     }, function( results, status ) {

        //         if ( status == google.maps.GeocoderStatus.OK ) {
        //             var p = results[ 0 ].geometry.location;
        //             var lat = p.lat( );
        //             var lng = p.lng( );
        //             createMarker( locationD, lat, lng );
        //         } else {
        //             if ( status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT ) {
        //                 nextAddress--;
        //                 delay++;
        //             } else {}
        //         }
        //         next( );
        //     } );
        // }

        // function createMarker( add, lat, lng ) {
        //     var contentString = '<div style="width:200px"><h4 style="margin:0px;padding:0px;">';
        //     contentString += add.name
        //     contentString += '</h4><br><p>'

        //     if ( add.web.indexOf( '@' ) < 0 ) {

        //         contentString += '<a href="http://' + add.web + '" target="_blank">'
        //     } else {

        //         contentString += '<a href="mailto:' + add.web + '">'
        //     }
        //     contentString += add.web + '</a></p></div>'
        //     var marker = new google.maps.Marker( {
        //         position: new google.maps.LatLng( lat, lng ),
        //         map: map,
        //         icon: imagePath
        //     } );

        //     google.maps.event.addListener( marker, 'click', function( ) {
        //         infowindow.setContent( contentString );
        //         infowindow.open( map, marker );
        //     } );

        //     bounds.extend( marker.position );

        // }
        // var nextAddress = 0;


        // function theNext( ) {
        //     if ( nextAddress < locations.length ) {
        //         setTimeout( geocodeAddress( locations[ nextAddress ], theNext ), delay );
        //         nextAddress++;

        //     } else {
        //         map.fitBounds( bounds );
        //     }
        // }


        // theNext( );


    }

} )