var $ = jQuery;
$( document ).on( "ready", function( ) {
    var altura = screen.height - 70;
    var widthScreen = screen.width
    if ( widthScreen > 768 ) {
        if ( widthScreen < 1400 ) {
            $.scrollify( {
                section: "section,footer",
                sectionName: false,
                interstitialSection: "",
                easing: "easeOutExpo",
                scrollSpeed: 1100,
                offset: 0,
                scrollbars: true,
                standardScrollElements: "#turismoContainer,#nobebas",
                setHeights: false,
                overflowScroll: false,
                before: function( ) {},
                after: function( ) {},
                afterResize: function( ) {},
                afterRender: function( ) {}
            } );
        }
        $( '#rev_slider_2_1' ).show( ).revolution( {
            dottedOverlay: "none",
            delay: 200,
            startwidth: 1350,
            startheight: altura,
            hideThumbs: 200,

            // simplifyAll:"off",

            // navigationType:"bullet",
            navigationArrows: "solo",
            navigationStyle: "default",

            touchenabled: "on",
            onHoverStop: "on",
            // nextSlideOnWindowFocus:"off",


            // keyboardNavigation:"off",

            navigationHAlign: "center",
            navigationVAlign: "bottom",
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: "left",
            soloArrowLeftValign: "center",
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: "right",
            soloArrowRightValign: "center",
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            // shadow:0,
            // fullWidth:"off",
            // fullScreen:"on",

            // spinner:"spinner4",

            // stopLoop:"off",
            // stopAfterLoops:-1,
            // stopAtSlide:-1,

            // shuffle:"off",


            // forceFullWidth:"on",
            // fullScreenAlignForce:"off",
            // minFullScreenHeight:"400",
            // hideTimerBar:"off",
            // hideThumbsOnMobile:"off",
            // hideNavDelayOnMobile:1500,
            // hideBulletsOnMobile:"off",
            // hideArrowsOnMobile:"off",
            // hideThumbsUnderResolution:0,

            // fullScreenOffsetContainer: ".headerwrap, .headertopwrap",
            // fullScreenOffset: "",
            // hideSliderAtLimit:0,
            // hideCaptionAtLimit:0,
            // hideAllCaptionAtLilmit:0,
            // startWithSlide:0             

        } );
    }
    $( document ).on( 'scroll', function( ) {


        if ( ( $( window ).scrollTop( ) + 200 >= $( "#vinosHome" ).offset( ).top ) && $( '#sliderVinos' ).is( ':hidden' ) ) {

            var heightscreen = $( window ).height( ) * .9
            var heightscreen = ( heightscreen > 800 ) ? 800 : heightscreen
            if ( heightscreen )
                $( '#sliderVinos' ).show( ).revolution( {
                    // startwidth:1350,
                    delay: 5000,
                    startheight: heightscreen,
                    navigationType: "both",
                    //navigationArrows:"nexttobullets",        
                    // navigationStyle:"default",                
                    touchenabled: "on",
                    onHoverStop: "on",
                    // soloArrowLeftHalign:"left",
                    // soloArrowLeftValign:"center",
                    // soloArrowLeftHOffset:20,
                    // soloArrowLeftVOffset:0,
                    // soloArrowRightHalign:"right",
                    // soloArrowRightValign:"center",
                    // soloArrowRightHOffset:20,
                    // soloArrowRightVOffset:0,
                    //                   navOffsetHorizontal:0,
                    //                   navOffsetVertical:20,                  
                    shadow: 1,
                    fullWidth: "off"
                } );

        }
    } )


} )