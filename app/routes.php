<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/destroy', function(){
	Cookie::queue(Cookie::forget('remember'));	
});
Route::get('/enviarmail', 'EspController@enviomail');
Route::get('/', 'EspController@index');
Route::post('/mayor', 'EspController@remember');
Route::get('{lg}/no', 'EspController@no');
// Route::group(array('before' => 'remember'), function()
// {
Route::get('/{lg}/', 'EspController@home');
Route::get('/{lg}/vinos', 'EspController@vinos');
Route::get('/{lg}/vinos/{linea}', 'EspController@detallevinos');
Route::get('/{lg}/familia', 'EspController@familia');
Route::get('/{lg}/bodega', 'EspController@bodega');
Route::get('/{lg}/vinedos', 'EspController@vinedos');
Route::get('/{lg}/visitas', 'EspController@turismo');
Route::get('/{lg}/contacto', 'EspController@contacto');
// Route::get('login', );
// });

