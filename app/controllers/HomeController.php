<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
public function index() {


      
//$cursos = Cursos::All();
    $js_array =  array('js/jquery.themepunch.revolution.min.js','js/jquery.themepunch.tools.min.js','/js/home.js');
      $data = [
          
            'active' =>'home',
            'js_array'=> $js_array
        ];

    return View::make('home',$data);
 }
// 	public function showWelcome()
// 	{

// 		return View::make('templates/layaout');
// 	}

// 	 public function showLogin() {
//         // show the form
//         return View::make('login');
//     }

//     public function doLogin () {

//         // validate the info, create rules for the inputs
//         $rules = array(
//             'username' => 'required|exists:users', // make sure the email is an actual email
//             'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
//         );

//         // run the validation rules on the inputs from the form
//         $validator = Validator::make(Input::all(), $rules);

//         // if the validator fails, redirect back to the form
//         if ($validator->fails()) {
//             return Redirect::to('login')
//                             ->withErrors($validator) // send back all errors to the login form
//                             ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
//         } else {

//             $username = Input::get('username');
//             $password = Input::get('password');
            
//             $user = User::where('username',$username)->first();
            
//             // create our user data for the authentication
//             $userdata = array(
//                 'username' => $username,
//                 'password' => $password
//             );

//             // attempt to do the login
//             if (Auth::attempt($userdata)) {
                
                
//                    return Redirect::to('admin');
                
               
                
//             } else {

//                 // validation not successful, send back to form 
//                 return Redirect::to('login');
//             }
//         }
//     }

//     public function doLogout() {
//         Auth::logout(); // log the user out of our application
//         return Redirect::to('login'); // redirect the user to the login screen
//     }

//     public function contacto() {
// $categoria = Categoria::all();
// //$cursos = Cursos::All();
//     $js_array =  array('js/contacto.js');
//  $data = [
//             'categoria' =>$categoria,
       
//             'active' =>'contacto',
//               'js_array'=> $js_array
         
//         ];
//     return View::make('contacto',$data);
// }

//     public function enviomail() {
//             $mensaje = null;
//             if(Input::get('tipo')=="contacto"){
//                 $data = array
//                     (
//                     'name'      =>  Input::get('nombre'),
//                     'email'     =>  Input::get('email'),
//                     'Teléfono'     =>  Input::get('telefono'),
//                     'asunto'   =>  Input::get('asunto'),
//                     'dedonde' => Input::get('dedonde'),
//                     'curso' => Input::get('curso')
//                     'msg'       =>  Input::get('msg')
//                     );
//                 $fromEmail  =   $data['email'];
//                 $fromName   =   $data['name'];
//                 $asunto   =   $data['asunto'];
//                 Mail::send('emails.email', $data, function($message) use ($fromName, $fromEmail,$asunto)
//                 {
//                     $message->replyTo($fromEmail, $fromName);
//                     $message->from('info@iscc.com.ar', 'ISCC');
//                     $message->to('info@iscc.com.ar', 'ISCC');
//                     $message->subject($asunto);
//                 });
//             }
//             if(Input::get('tipo')=="interesa"){
//                 $data = array
//                     (
//                     'name'       =>  Input::get('nombre'),
//                     'email'      =>  Input::get('email'),
//                     'Teléfono'   =>  Input::get('telefono'),
//                     'asunto'     =>  Input::get('asunto'),
//                     'provincia'  =>  Input::get('provincia'),
//                     'ciudad'     =>  Input::get('ciudad'),
//                     'conocistes' =>  Input::get('conocistes'),
//                     'interesa'   =>  Input::get('interesa')
//                     );
//                 $fromEmail  =   $data['email'];
//                 $fromName   =   $data['name'];
//                 $asunto   =   $data['asunto'];
//                 Mail::send('emails.interesa', $data, function($message) use ($fromName, $fromEmail,$asunto)
//                 {
//                     $message->replyTo($fromEmail, $fromName);
//                     $message->from('info@iscc.com.ar', 'ISCC');
//                     $message->to('info@iscc.com.ar', 'ISCC');
//                     $message->subject($asunto);
//                 });
//             }

//         echo "oka"    ;  
                  
// }



}



