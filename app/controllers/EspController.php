<?php

class EspController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
public function index() {
	$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	
	if(Cookie::has('remember')){
		return Redirect::to("/".$lang);
	}else{
		$imgrand = rand(0,3);
      	$imagenArray = array('tonel','toneles','vinedos_nieve','fondo_inicio');
      	$imagenIni = $imagenArray[$imgrand];
		$data = [
			'lang'=>$lang,
			'imagenIni' =>$imagenIni
		];
	    
		return View::make('inicio_'.$lang, $data);
	}
}
public function no($lg) {
	if(!$lg){
	$lg ='es';
}
	$data = [
      		
      		'lang' => $lg,
          	'page'=>'home',
            'active' =>'home'
        ];
		return View::make('no_'.$lg, $data);
	
}
public function remember(){
	$lg = Input::get('lg');
	$remember = Input::get('remember');
	
	$minutos = time()+ (24 * 60 *30);
	$minutos2 = time()+ (24 * 60 );
	if($remember){		
		$cookie = Cookie::make('remember', $lg, $minutos);
		return Redirect::to("/".$lg."/")->withCookie($cookie);
	}else{
		$cookie = Cookie::make('remember', $lg, $minutos2);
		return Redirect::to("/".$lg."/")->withCookie($cookie);
		
	}
	
	
	
	
}

public function home($lg) {

if(!$lg){
	$lg ='es';
}
switch ($lg) {
	case 'es':
		$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboracion de nuestros vinos. Descubra la pasión con que creamos nuestros vinos...  ';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboracion de nuestros vinos. Descubra la pasión con que creamos nuestros vinos...  ';
		break;
}


//$cursos = Cursos::All();
    $js_array =  array('js/jquery.themepunch.revolution.min.js','js/jquery.themepunch.tools.min.js','/js/home.js');
      $data = [
      		'title'=>$title,
      		'descripcion'=>$descripcion,
      		'lang' => $lg,
          	'page'=>'home',
            'active' =>'home',
            'js_array'=> $js_array
        ];

    return View::make('home_'.$lg,$data);
 }
 public function familia($lg) {
 	if(!$lg){
	$lg ='es';
}
switch ($lg) {
	case 'es':
			$title = 'La Familia  :: Bodega y Vinos Roca' ;
		$descripcion = 'En la Familia compartimos una gran pasión que nos mueve día a día, lograr las mejores uvas y criar excelentes vinos. ';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboracion de nuestros vinos. Descubra la pasión con que creamos nuestros vinos...  ';
		break;
}
	$js_array =  array('js/jquery.themepunch.revolution.min.js','js/jquery.themepunch.tools.min.js');
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
				'lang' => $lg,
	          	'page'=>'La Familia',
	            'active' =>'familia',
	            'js_array'=> $js_array
	        ];

    return View::make('familia_'.$lg,$data);
 }

 public function bodega($lg) {
if(!$lg){
	$lg ='es';
}
switch ($lg) {
	case 'es':
		$title = 'La Bodega  :: Bodega y Vinos Roca'   ;
		$descripcion = ' informacion sobre La Bodega, de Bodega Roca en San Rafael. Descubra la historia, la actualidad, los procesos, fotos, etc de Bodega Roca ';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboramos de nuestros vinos. Descubra la pasión con que creamos nuestros vinos.  ';
		break;
}
 	$css_array = array('js/ligthGallery/css/lightgallery.min.css');
	$js_array =  array('js/ligthGallery/js/lightgallery-all.min.js','js/bodega.js');
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
	      		'lang' => $lg,
	          	'page'=>'La Bodega',
	            'active' =>'bodega',
	            'css_array'=> $css_array,
	            'js_array'=> $js_array
	        ];

    return View::make('bodega_'.$lg,$data);
 }
  public function vinedos($lg) {
if(!$lg){
	$lg ='es';
}

switch ($lg) {
	case 'es':
		$title = 'Nuestros Viñedos :: Bodega y Vinos Roca'   ;
		$descripcion = 'Los invitamos a conocer nuestros viñedos en San Rafael , Mendoza. Situados al pie de la Cordillera de Los Andes.   ';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboramos de nuestros vinos. Descubra la pasión con que creamos nuestros vinos.  ';
		break;
}
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
	      		'lang' => $lg,
	          	'page'=>'Nuestros Viñedos',
	            'active' =>'vinedos'
	        ];

    return View::make('vinedos_'.$lg,$data);
 }

   public function turismo($lg) {
if(!$lg){
	$lg ='es';
}

switch ($lg) {
	case 'es':
		$title = 'Visitas Guiadas :: Bodega y Vinos Roca'   ;
		$descripcion = 'Descubra la pasión con que elaboramos nuestros vinos. Visitas guidas, tours por la bodega, degustaciones, visita al viñedo, visitas educativas.';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboramos de nuestros vinos. Descubra la pasión con que creamos nuestros vinos.  ';
		break;
}
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
	      		'lang' => $lg,	
	          	'page'=>'Turismo, visítanos',
	            'active' =>'turismo'
	        ];

    return View::make('turismo_'.$lg,$data);
 }
 public function vinos($lg) {
if(!$lg){
	$lg ='es';
}

switch ($lg) {
	case 'es':
		$title = 'Nuestros Vinos :: Bodega y Vinos Roca'   ;
		$descripcion = 'Vinos Roca, conozca todas nuestras lineas de vinos y sus varietales. Descripcion detalla de cada uno';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboramos de nuestros vinos. Descubra la pasión con que creamos nuestros vinos.  ';
		break;
}
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
	      		'lang' => $lg,	
	          	'page'=>'Turismo, visítanos',
	            'active' =>'turismo'
	        ];

    return View::make('vinos_'.$lg,$data);
 }

 public function detallevinos($lg,$linea) {
	if(!$lg){
		$lg ='es';
	}
	$pagina = 'vinos_'.$linea."_".$lg;
	switch ($lg) {
	case 'es':
		$title = 'Vinos '.$linea.' :: Bodega y Vinos Roca'   ;
		$descripcion = 'Vinos Roca, conozca todas nuestras lineas de vinos y sus varietales. Descripcion detalla de cada uno';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboramos de nuestros vinos. Descubra la pasión con que creamos nuestros vinos.  ';
		break;
}
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
	      		'lang' => $lg,	
	          	'page'=>'Turismo, visítanos',
	            'active' =>'turismo'
	        ];

   	 return View::make($pagina,$data);
 }

 public function contacto($lg) {
if(!$lg){
	$lg ='es';
}
	switch ($lg) {
	case 'es':
		$title = 'Contactenos :: Bodega y Vinos Roca'   ;
		$descripcion = 'Contactese con nosotros. Bodega y Vinos Roca ';
		break;
	case 'en':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
		$descripcion = " Welcome to Roca's Cellar in San Rafael . Invite you to visit our Cellar , our vineyards and our wines we produce . Discover the passion with which we create our wines.  ";
		break;
	case 'pt':
		$title = "Roca's Cellar and  Wines  in San Rafael, Mendoza, Argentina";
	case 'pt':
		$descripcion = " Bem-vindo à Adega do Roca em San Rafael . Convidamos você a visitar nossa adega , nossas vinhas e os nossos vinhos produzimos . Descubra a paixão com que criámos os nossos vinhos...  ";

		break;
	default:
			$title = ' Bodega y Vinos Roca San Rafael, Mendoza, Argentina '   ;
		$descripcion = ' Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboramos de nuestros vinos. Descubra la pasión con que creamos nuestros vinos.  ';
		break;
}
$js_array =  array('js/contacto.js');
	      $data = [
	      'title'=>$title,
      		'descripcion'=>$descripcion,
	      		'lang' => $lg,	
	          	'page'=>'Turismo, visítanos',
	            'active' =>'turismo',
	            'js_array' => $js_array
	        ];

    return View::make('contacto_'.$lg,$data);
 }


    public function enviomail() {
            $mensaje = null;
          
                $data = array
                    (
                    'nombre'      =>  Input::get('nombre'),
                    'apellido' =>  Input::get('apellido'),
                    'email'     =>  Input::get('email'),
                    'Teléfono'     =>  Input::get('telefono'),
                    'aquien'   =>  Input::get('aquien'),
                    'msg'       =>  Input::get('msg')
                    );
                $fromEmail  =   $data['email'];
                $fromName   =   $data['nombre']. ' '. $data['apellido'];
                $aquien   =   $data['aquien'];
                Mail::send('emails.email', $data, function($message) use ($fromName, $fromEmail,$aquien)
                {
                    $message->replyTo($fromEmail, $fromName);
                    $message->from($aquien);
                    $message->to($aquien);
                    $message->subject('Consulta desde la Web');
                });
           
        echo "oka"    ;  
                  
}

}



