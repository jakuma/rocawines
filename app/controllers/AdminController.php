<?php

class AdminController  extends BaseController {

	public function listCursos() {

		    //datos que se envian a la vistas
        //$cursos = Cursos::paginate(30);

         $query = sprintf("
            select * from cursos c, categoria_cursos cc where cc.idcat=c.categoriaId ");
        // echo $query;
        // exit();


        $cursos = DB::select($query);
        $data = [

            'cursos' => $cursos,
        ];
        //llamado a la vista blade  
        return View::make('admin/admin', $data);

	}
	public function nuevocurso() {

            $categoria = Categoria::all();
		    //datos que se envian a la vistas
		    $data=[
                'categoria'=>$categoria
            ];
       
        return View::make('admin/crear_cursos', $data);

	}

    public function crearcurso() {
            


            $curso = new Cursos();
           
            $curso->Nombre = Input::get('nombre');
            $curso->Descripcion = Input::get('Descripción');
            $curso->Contenido = Input::get('contenido');
            $curso->ProgramaEstudio = Input::get('programaEstudio');
            $curso->Titulo = Input::get('titulo');
            $curso->Duracion = Input::get('duracion');
            $curso->Modalidad = Input::get('modalidad');
            $curso->Requisitos = Input::get('requisitos');
            $curso->Inscripcion = Input::get('inscripcion');
            $curso->Valor = Input::get('valor');
            $curso->DiasHorario = Input::get('diasHorario');
            $curso->categoriaId = Input::get('categoriaId');
            $curso->tipo = Input::get('tipo');

            //datos que se envian a la vistas
                  $curso->save();
           

             //Session::flash('message', "Se agrego un nuevo Curso");
            return Redirect::to('admin');

    }

public function editcurso($id) {


            $curso = Cursos::find($id);
            //datos que se envian a la vistas
            $categoria = Categoria::all();
            
            $data=[
                'categoria'=>$categoria,          
                'curso'=>$curso
            ];
       
        return View::make('admin/edit_curso', $data);

    }

     public function Updatecurso($id) {
           
            $curso = Cursos::find($id);

            $curso->Nombre = Input::get('nombre');
            $curso->Descripcion = Input::get('Descripción');
            $curso->Contenido = Input::get('contenido');
            $curso->ProgramaEstudio = Input::get('programaEstudio');
            $curso->Titulo = Input::get('titulo');
            $curso->Duracion = Input::get('duracion');
            $curso->Modalidad = Input::get('modalidad');
            $curso->Requisitos = Input::get('requisitos');
            $curso->Inscripcion = Input::get('inscripcion');
            $curso->Valor = Input::get('valor');
            $curso->DiasHorario = Input::get('diasHorario');
            $curso->categoriaId = Input::get('categoriaId');
            $curso->tipo = Input::get('tipo');
            //datos que se envian a la vistas
            $curso->save();
             Session::flash('message', "Se agrego un nuevo Curso");
            return Redirect::to('admin');

    }

    public function fotocurso($id)
    {
        $image=false;
        $myfile="upload/".$id.".jpg";
        if(File::exists($myfile)){
            $image=true;
        }
        $data =[
            'id'=>$id,
            'image'=>$image
        ];

        return View::make('admin/foto_cursos', $data);
    }

    public function upfoto($id) {
         $image = Input::file('foto');
            if(isset($image)){
               
                $savepath = 'upload/';
                $filename = $image->getClientOriginalName();
                $image->move($savepath, $id.".jpg");
                $curso = Cursos::find($id);
                $curso->foto = 1;
                $curso->save();

            }
                return Redirect::to('admin');
    }

    public function categorias()
    {
            if(Input::get("msj")!="" ){
                $msj=Input::get("msj");
            }else{
                $msj="";
            };
           $categoria = Categoria::all();
            //datos que se envian a la vistas
            $data=[
                'categorias'=>$categoria,
                'msj'=>$msj
            ];
       
        return View::make('admin/categorias', $data);
    }
 public function nueva_cat()
    {   
       $data=[];
        return View::make('admin/nueva_cat',$data);
    }

    public function crear_cat()
    {
         $cat = new Categoria();

            $cat->categoria = Input::get('categoria');
            $cat->save();
              return Redirect::to('admin/cat');
    }


      public function edit_cat($id)
    {
        $cat = Categoria::find($id);


        $data = [
            'cat' =>$cat 
        ];
            return View::make('admin/edit_cat', $data);
    }
    

    public function update_cat($id)
    {
  
        $cat = Categoria::find($id);
      $cat->categoria = Input::get('categoria');
            $cat->save();
       
       return Redirect::to('admin/cat');

    }

    public function delete_curso($id){
               $curso = Cursos::find($id);
                $curso->delete();
                  return Redirect::to('admin');

    }
     public function delete_cat($id){
               $cat = Categoria::find($id);
               $curso = DB::table('cursos')->where('idcat',"=",$id);

               if(!isset($curso)){

                $cat->delete();
                $msj="";
                }else {

                  $msj="Imposible Borrar hay cursos con esta categoria (".$cat->categoria.") Asociada";
                }
            
                return Redirect::to('admin/cat?msj='.$msj);

    }
}



