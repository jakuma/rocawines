<?php

class CursosController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

public function index() {

    $categoria = Categoria::all();
    $sql = "select *  from cursos c, categoria_cursos cc  where c.foto=1 and cc.idcat = c.categoriaId limit 0,9";
    $cursos=DB::select($sql);
    //$cursos = Cursos::All();
    $js_array =  array('js/jquery.themepunch.revolution.min.js','js/jquery.themepunch.tools.min.js','/js/home.js');
    $data = [
      'categoria' =>$categoria,
      'cursos'=>$cursos,
      'js_array'=> $js_array
      ];
    return View::make('home',$data);
}


 public function listarCursosPorCat($cat){
  $categoria = Categoria::all();
  $catName  = str_replace("-"," ", $cat);
   $sql = "select * from categoria_cursos where categoria='".$catName ."'";
   $cat_id = DB::select($sql);
  
 
  $sql = "select * from cursos where foto=1 and categoriaId=".$cat_id[0]->idcat;
  $cursos=DB::select($sql);

  $js_array ="";
       $data = [
       'categoria' =>$categoria,
        'cursos'=>$cursos,
        'active' =>'cursos',
        "catName" =>$cat_id[0]->categoria,
        "urlCat" =>$cat
        ];
return View::make('cursos_por_categoria',$data);
}

 public function detalle($cat,$url){
  $dedonde = Input::get("dedonde");
  if(!isset($dedonde)){
     $dedonde ="web";
  }
  $categoria = Categoria::all();
  $dir = explode("-",$url);
  $id = $dir[count($dir)-1];
  $query = sprintf("
    select * from cursos c, categoria_cursos cc where c.idcurso=%s and cc.idcat=c.categoriaId ",$id);
  // echo $query;
  // exit();


  $curso = DB::select($query);
  $js_array = array('/js/home.js');
       $data = [
       'categoria' =>$categoria,
            'curso'=>$curso[0],
             'active' =>'cursos',
             'dedonde'=>$dedonde
        ];
return View::make('curso_detalle',$data);
}

 

}



