@extends ('templates/inicio')
@section ('content')
  <div id="inicio">
    <img src="{{ URL::to('/') }}/images/{{$imagenIni}}.jpg" class="fade-in one"  alt="">
    <div id="box_inicio" class="fade-in two">
    	<div align="center">
    		<img src="./images/logo_blanco_vertical.png" alt="" class="logoR">
    	</div>
        <div class="info">
        <div class="idioma">
           <!-- <a href="">English </a> - <a href="">Português</a> -->
        </div>
    	<div class="pregunta">
    		¿ Sos mayor de 18 años ?
    	</div>
    	<div align="center" style="margin-bottom:20px">
    		<div class="botones" id="mayorEdad">
            SI
            </div>
    		<div class="botones" id="menorEdad" rel="{{URL::to('/')}}/{{$lang}}/no">NO</div>
    	</div>
    	<div id="recuerda" align="center">
        {{Form::open(array('url' => '/mayor', 'method' => 'post'))}}
    		<div class=""><input type="checkbox" name="remember" value="true">
    			Recuérdame la próxima vez
    		</div>
            <input type="hidden" value="{{$lang}}" name="lg" id="lang" >

        {{ Form::close() }}
    	</div>
        </div>
    </div>
    <div id="box_url">
        <a href="{{URL::to('es/')}}/familia"> Familia</a> | 
        <a href="{{URL::to('es/')}}/bodega"> Bodega</a> |
        <a href="{{URL::to('es/')}}/vinos"> Vinos</a> |
         <a href="{{URL::to('es/')}}/vinedos"> Viñedos</a> | 
        <a href="{{URL::to('es/')}}/visitas"> Visitas</a> |
        <a href="{{URL::to('es/')}}/contacto"> Contacto</a> |
    </div>

  </div>
  @stop