@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">
	<h1 class="tituloPagina vinos fadeIn">
	Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
</section>
<section id="nuestrosvinos" class="detalleVinos">
	<div align="center"><h2>Alfredo Roca  Dedicacion Personal</h2></div>
	<div class="texto col-md-8 col-md-offset-2" align="center">
		<p>
			La dedicación personal de quienes aportamos el trabajo en nuestros viñedos y en nuestra bodega hace posible este vino que es la expresión de nuestra familia hacia el arte de hacer vinos.
			Premiados en el último Vinus 2008 con doble Medalla de Oro y Medalla de Plata.<p>
			</div>
			<div class="clearfix"></div>
		</section>
		<section id="losvinos">
			<div class="col-xs-12 col-md-12">
				<div class="col-md-6 col-xs-12 itemvino">
					<div class="imgVino col-md-5">
						<img src="{{URL::to('/')}}/images/vinos/dedicacion/dedicacion-personal-bonarda.jpg" alt="">
					</div>
					<div class="textovino col-md-7">
						<h3>Bonarda </h3>
						<p>Intenso y profundo color rojo rubí con tonos negruzcos. este vino posee aromas que recuerdan a frambuesa y guinda, los que se mezclan con notas delicadas de vainilla y especiado aportados por el roble en su paso por barricas. En boca su expresión frutal vuelve a sorprender acompañada de una suave textura que deja un prolongado final de boca.</p>
						<div class="fichapdf">
							<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_dp_bonarda.pdf" class="btn btn-roca">
								Ficha Técnica
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12 itemvino">
					<div class="imgVino col-md-5">
						<img src="{{URL::to('/')}}/images/vinos/dedicacion/dedicacion-personal-cabernet-sauvignon.jpg" alt="">
					</div>
					<div class="textovino col-md-7">
						<h3>Cabernet Sauvignon</h3>
						<p>Intenso color rojo de notas rubí. Aromas que recuerdan ciruelas, casis, que se mezclan con notas delicadas de vainilla y especias aportadas por su paso por barricas. Se destaca por su sedosidad y taninos amables, pero presentes. Posee gran persistencia en boca y un muy buen potencial de guarda</p>
						<div class="fichapdf">
							<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_dp_cabernet.pdf" class="btn btn-roca">
								Ficha Técnica
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12 itemvino">
					<div class="imgVino col-md-5">
						<img src="{{URL::to('/')}}/images/vinos/dedicacion/decicacion-personal-sauvignon-blanc.jpg" alt="">
					</div>
					<div class="textovino col-md-7">
						<h3>Sauvignon Blanc</h3>
						<p>Atractivo color amarillo verdoso con tonos plateados. Este Sauvignon Blanc brinda una gran intensidad aromática en donde se destacan notas cítricas que recuerdan al pomelo y a la lima, los cuales se mezclan con aromas de frutas tropicales y durazno. En boca es persistente y fresco gracias a su equilibrada acidez.</p>
						<div class="fichapdf">
							<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_dp_sauvignon.pdf" class="btn btn-roca">
								Ficha Técnica
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12 itemvino">
					<div class="imgVino col-md-5">
						<img src="{{URL::to('/')}}/images/vinos/dedicacion/dedicacion-personal-chardonnay.jpg" alt="">
					</div>
					<div class="textovino col-md-7">
						<h3>Chardonnay</h3>
						<p>Color dorado ámbar. En su nariz aparecen notas de miel, pasas y tostadas que en boca se suman a su dulzura y sabores que recuerdan a nuez. Untuoso con sensación cremosa.</p>
						<div class="fichapdf">
							<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_dp_chardonay.pdf" class="btn btn-roca">
								Ficha Técnica
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</section>
		@Stop