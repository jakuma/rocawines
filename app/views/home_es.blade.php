@extends ('templates/layaout')
@section ('content')

<div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:#222;padding:0px;position:relative">
	<!-- START REVOLUTION SLIDER 4.6.5 fullscreen mode -->
	<div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;position:relative; height:100%;">
		<ul>	<!-- SLIDE  -->
		<li data-transition="fade" data-slotamount="7" data-masterspeed="300"   data-saveperformance="off"  data-title="success">
			<!-- MAIN IMAGE -->
			<img src="{{ URL::to('/') }}/images/slider1.jpg"  alt=""  >
			<!-- LAYERS -->
		<div class="tp-caption cienCosechas tp-sfl rs-parallaxlevel-0"
				data-x="left" data-hoffset="120"
				data-y="top" data-voffset="494"
				data-speed="1000"
				data-start="1000"
				data-easing="Power3.easeInOut"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				style="z-index: 6;">
				Cien Cosechas

			</div> 

			<!-- LAYER NR. 5 -->
			<div class="tp-caption cienCosechas_text tp-fade  rs-parallaxlevel-0"
				data-x="left" data-hoffset="125"
				data-y="top" data-voffset="584"
				data-speed="500"
				data-start="1500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="width:30% !important"
				>
						Celebramos las 100 cosechas de nuestra Familia abocada enteramente a los viñedos. 
					
					
				</div>
					<div class="tp-caption botonCien  tp-fade  rs-parallaxlevel-0"
				data-x="left" data-hoffset="125"
				data-y="top" data-voffset="624"
				data-speed="500"
				data-start="1500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="width:30% !important"
				align="left"
				>
						<a href="{{URL::to('/')}}/pdf/ficha_enologica_cien_cosechas.pdf" style="margin: 20px 0" class="btn btn-roca">
							<i class="fa fa-angle-double-right" aria-hidden="true"></i> Ficha Técnica
						</a>
					
					
				</div>
			
			
		</li>
		
		<!-- SLIDE  -->
		
	</ul>
		<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
	</div>
</div>
<div class="fotomobiles">
	<img src="{{ URL::to('/') }}/images/slider1.jpg"  alt=""  >

<div class="h1mobile">
	<h2>Cien Cosechas</h2>
	<p>
	Celebramos las 100 cosechas de nuestra Familia abocada enteramente a los viñedos. </p>
<!-- 	<a href="{{URL::to('/')}}/pdf/ficha_enologica_cien_cosechas.pdf" style="margin: 20px 0" class="btn btn-roca">
							<i class="fa fa-angle-double-right" aria-hidden="true"></i> Ficha Técnica
						</a> -->
					
</div>
</div>
<section id="familia-home">
	<div class="container">
	
		
			<div class="col-md-6 col-xs-12">
				<img src="{{ URL::to('/') }}/images/familia.jpg" class="fotofamilia" alt="La Familia - Bodega Roca">
			</div>
			<div class="col-md-6 col-xs-12 col-md-offset-0 col-xl-offset-1 col-xl-4 ">
				<div class="section_heading">
					<h1><i>"Dedicacion Familiar Vinos con Alma"</i></h1>
					<div class="lineBlue"></div>
				</div>
				<div class="parrafoFamilia">
					<p>En la Familia compartimos una gran pasión que nos mueve día a día, lograr las mejores uvas y criar excelentes vinos.</p>
					<p>Hoy, la cuarta generación de nuestra familia ha convertido esta pasión en un estilo de vida. El cuidado personal y profesional, sumado a las características de nuestro terroir, aportan a nuestros vinos características únicas e inigualables.
</p>
				<div class="col-md-12" align="right" style="margin-top:10px">
					<a href="{{URL::to($lang.'/familia')}}" class="btn btn-roca"> más info >></a>
				</div>
				</div>
			</div>
				
	
	<div class="clearfix"></div>
	</div>
</section>

<section id="vinosHome">
		<div id="sliderVinos_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color:#222;padding:0px;position:relative">
	<!-- START REVOLUTION SLIDER 4.6.5 fullscreen mode -->
	<div id="sliderVinos" class="rev_slider" style="display:none; position:relative">
		<ul style="position:relative">	<!-- SLIDE  -->
		<li data-transition="fade" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off"  data-title="success">
			<!-- MAIN IMAGE -->
			<img src="{{ URL::to('/') }}/images/slider_home_2/fondo_nuri.jpg"  alt="bg_header_bottom4" >
	
			<div class="tp-caption black tp-fade tp-resizeme rs-parallaxlevel-0"
				
				data-x="left" data-hoffset="-650"
				data-y="top" data-voffset="-100"
				data-speed="300"
				data-start="800"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0;  "><div class="key2seccess_txt">
				
					<img id="H-sld2-3" src="{{ URL::to('/') }}/images/slider_home_2/slider_3_nuri.png" alt="">
				</div>
			</div>
			<div class="tp-caption cienCosechas  tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="650"
				data-y="180"
				data-speed="500"
				data-start="1500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0;color:#222; font-zise:65px;  white-space: nowrap;">
				<div class="key2seccess_txt">
				NURI
				<!-- 
					<img src="{{ URL::to('/') }}/images/slider_home_2/dulce_natural.png" style="" alt=""> -->
				</div>
			</div>
			<div class="tp-caption cienCosechas_text tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="650"
				data-y="250"
				data-speed="500"
				data-start="1500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0; width:30% !important; color:#222" >
				<div class="key2seccess_txt">
				Un elegante vino rosado de Malbec elaborado a partir de una precisa selección de uvas y una expresión de amor y reconocimiento de sus hijos hacia su madre, Gladys Nuri (mujer de Alfredo Roca).
				
				</div>
			</div>
			<div class="tp-caption linkSlider tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="650"
				data-y="400"
				data-speed="300"
				data-start="1600"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0; width:30% !important" ><div class="key2seccess_txt">
				<a href="{{URL::to('/')}}/pdf/ficha_enologica_nuri.pdf" target="blank_">Ficha Tecnica
				<i class="fa fa-chevron-right"></i>
				</a>
				
					</div>
			</div>
	 </li>
		<li data-transition="fade" data-slotamount="5" data-masterspeed="800"  data-saveperformance="off"  data-title="success">
			<!-- MAIN IMAGE -->
			<img src="{{ URL::to('/') }}/images/slider_home_2/slider_2_bg_2.jpg"  alt="bg_header_bottom4" >
	
			<div class="tp-caption black tp-fade tp-resizeme rs-parallaxlevel-0" 
				
				data-y="0"
				data-speed="300"
				data-start="500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0;left:0px !important;  white-space: nowrap;"><div class="key2seccess_txt">
				
					<img id="H-sld2-1" src="{{ URL::to('/') }}/images/slider_home_2/slider_2_Line_Roca.png" style="width:auto;height:600px !important;" alt="Vino Roca">
				</div>
			</div>
					<div class="tp-caption black tp-sfr tp-resizeme rs-parallaxlevel-0"
				data-x="right" data-hoffset="-50"
				data-y="top" data-voffset="250"
				data-speed="300"
				data-start="800"
				data-speed="300"
				data-start="1200"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="500"
				style="z-index: 9; bottom:0;  white-space: nowrap;"><div class="key2seccess_txt">
				
					<img src="{{ URL::to('/') }}/images/slider_home_2/dulce_natural.png" style="" alt="">
				</div>
			</div>
				<div class="tp-caption linkSlider tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="640"
				data-y="500"
				data-speed="300"
				data-start="1600"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0; width:30% !important" ><div class="key2seccess_txt">
				<a href="{{URL::to('/')}}/vinos/roca">Mas info 
				<i class="fa fa-chevron-right"></i>
				</a>
				
					</div>
			</div>
	 </li>
		<li data-transition="fade" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off"  data-title="success">
			<!-- MAIN IMAGE -->
			<img src="{{ URL::to('/') }}/images/slider_home_2/fondo_roca.jpg"  alt="bg_header_bottom4" >
	
			<div class="tp-caption black tp-fade tp-resizeme rs-parallaxlevel-0"
				
				data-x="left" data-hoffset="30"
				data-y="top" data-voffset="-180"
				data-speed="300"
				data-start="800"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0;  "><div class="key2seccess_txt">
				
					<img id="H-sld2-2" src="{{ URL::to('/') }}/images/slider_home_2/Slider2_Merlo_ Rose.png" style="width:auto;height:30% !important;" alt="">
				</div>
			</div>
			<div class="tp-caption cienCosechas tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="50"
				data-y="140"
				data-speed="500"
				data-start="1500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0;  white-space: nowrap;">
				<div class="key2seccess_txt">
				Merlot Rosé
				<!-- 
					<img src="{{ URL::to('/') }}/images/slider_home_2/dulce_natural.png" style="" alt=""> -->
				</div>
			</div>
			<div class="tp-caption cienCosechas_text tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="50"
				data-y="210"
				data-speed="500"
				data-start="1500"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0; width:30% !important" >
				<div class="key2seccess_txt">
					Fresco aroma frutal que recuerda a frutillas y frambuesas que también se despliegan en boca. 
				
				</div>
			</div>
			<div class="tp-caption linkSlider tp-left tp-resizeme rs-parallaxlevel-0"
				data-x ="50"
				data-y="320"
				data-speed="300"
				data-start="1600"
				data-easing="Power3.easeInOut"
				data-splitin="none"
				data-splitout="none"
				data-elementdelay="0.1"
				data-endelementdelay="0.1"
				data-endspeed="300"
				style="z-index: 9; bottom:0; width:30% !important" ><div class="key2seccess_txt">
				<a href="{{URL::to('/')}}/vinos/fincas">Mas info 
				<i class="fa fa-chevron-right"></i>
				</a>
				
					</div>
			</div>
	 </li>
		<!-- SLIDE  -->
		
	</ul>
	<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
</div>
	
	</div>
</section>
<section id="mapaConteiner">
		<div class="titulo"><h2>Nuestra Bodega</h2></div>

	<div id="map">
	</div>
</section>

<section id="turismoContainer">
	<div class="col-md-4 col-xs-12 col-md-offset-4" align="center">	
		<h3>	TURISMO</h3>
		<h2>Visita Nuestra Bodega</h2>
		<button>Más Información</button>
	</div>
</section>
<section id="nobebas">
	<img src="{{ URL::to('/') }}/images/beberconmoderacion.jpg" alt="">
</section>
@stop