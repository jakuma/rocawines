@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">
	<h1 class="tituloPagina vinos fadeIn">
		Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
</section>
<section id="nuestrosvinos" class="detalleVinos">
	<div align="center"><h2>Alfredo Roca  Linea Roca</h2></div>
	<div class="texto col-md-8 col-md-offset-2" align="center">
		<p>
Conoce nuestro suelo, descubre nuestros aromas, admira nuestro paisaje.<p>
	</div>
	<div class="clearfix"></div>
</section>

<section id="losvinos">
	<div class="col-xs-12 col-md-12">
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/roca/roca-malbec.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Roca Malbec - Merlot</h3>
				<p>Color rojizo vivo, con aromas de ciruelas y frutos rojos. El aporte de taninos dulces del Malbec sumados a la delicadeza del Merlot, dan por resultado un vino elegante e intenso.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_roca_malbec.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/roca/roca-chardonay.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Roca Chenin Chardonay</h3>
				<p>Vino de color amarillo liviano con tonos verdosos. 
Expresa los delicados aromas a duraznos propios de la variedad Chenin, en boca la personalidad del Chardonnay aporta untuosidad y carácter que se realzan gracias a la frescura de su balanceada acidez.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_roca_chenin.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/roca/roca-espumante.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Roca Dulce Natural Espumante</h3>
				<p>Vino de color amarillo liviano con tonos verdosos. 
De color amarillo pálido, posee burbujas finas y persistentes. Su aroma evoca dejos de durazno, cítricos y flores blancas que se mezclan con finas notas de miel que le aportan sofisticación.  En boca se destaca un sabor dulce, delicado, refrescante y de buen volumen. Un espumante de gran calidad y expresión, ideal para aquellos paladares que huyen de vinos con cuerpo o acidez intensa.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_roca_dulce_natural.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		
	</div>
	<div class="clearfix"></div>
</section>
@Stop