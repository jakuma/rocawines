@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<h1 class="tituloPagina vinos fadeIn">
		Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">
</section>
<section id="nuestrosvinos" class="detalleVinos">
	<div align="center"><h2>Alfredo Roca Fincas</h2></div>
	<div class="texto col-md-8 col-md-offset-2" align="center">
		<p>Hay una tradición que nos guía. Hay una pasión que nos une. Hay un placer que nos convoca día a día a nuestros viñedos.
<b>Todo momento es irrepetible, convertilo en inolvidable.</b><p>
	</div>
	<div class="clearfix"></div>
</section>

<section id="losvinos">
	<div class="col-xs-12 col-md-12">
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-malbec.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Malbec</h3>
				<p>Presenta color rojo bordó, de muy buena concentración y equilibrio en boca, sobresaliendo los taninos dulces y aromáticos. Con sabores que recuerdan a la mermelada de ciruela.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_malbec.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-cabernet-sauvignon.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Cabernet Sauvignon</h3>
				<p>Presenta color rojo bordó, de muy buena concentración y equilibrio en boca, sobresaliendo los taninos dulces y aromáticos. Con sabores que recuerdan a la mermelada de ciruela.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_cabernet.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-pinot-noir.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Pinot Noir</h3>
				<p>Color de tonos rojos suaves. Intensa expresión aromática con notas florales dulces, cherry y delicada presencia de roble gracias a su añejamiento de 8 meses en barrica. Con sabores de frutos rojos y ciruela madura. A pesar de su suavidad, la presencia de importantes taninos se hace sentir.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_pinot.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-syrah.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Syrah</h3>
				<p>Atractivo e intenso color rojo de tonos violáceos. Descomunal sabor a frutas oscuras y ciruelas con un muy buen potencial de guarda a pesar de estar en óptimo momento para beberlo.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_syrah.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-merlot.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Merlot</h3>
				<p>Fermentación meloláctica cumplida. Vino armónico de taninos suaves y redondos, con notas a fruta dulce, cuerpo delicado y aterciopelado.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_merlot.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-chardonnay.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Chardonnay</h3>
				<p>Vino blanco de gran complejidad en donde se combinan los aromas frutales propios de la variedad. Suave en boca y a su vez gran intensidad de sabor.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_chardonnay.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
	
	<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-tocai.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Tocai</h3>
				<p>Variedad de origen italiano. Vino blanco de gran personalidad que conjuga aromas florales y frutales con fresca acidez.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_tocai.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-chenin.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Chenin</h3>
				<p>Fino y sutil con aromas a durazno, complementan un fresco y enérgico paladar, con presencia de una justa acidez propia de la variedad, eminente de los grandes vinos Chenin de San Rafael.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_chenin.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>

		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/fincas/fincas-rose.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Fincas Merlot Rosé</h3>
				<p>Atractivo color rosado de vivos tonos violetas. Fresco aroma frutal que recuerda a frutillas y frambuesas las que también se despliegan en boca. Presenta una acidez fresca, agradable con muy buen balance y final de boca amable de suave dulzor, prevalecen notas de frutos rojos, guindas y cereza.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_fincas_rose.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 ">
		
		</div>
	</div>
</div>
</section>
