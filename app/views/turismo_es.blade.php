@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img src="{{ URL::to('/') }}/images/internas/Uvas-Cabernet.jpg" alt="">
	<h1 class="tituloPagina turismo fadeIn">
	Turismo
	</h1>
	<div class="textosup textobtur fadeIn ">
		Descubra la pasión con que elaboramos nuestros vinos.
	</div>
	<!-- <img src="{{ URL::to('/') }}/images/internas/turismo_int.jpg" alt=""> -->
</section>
<section class="infotour">
	<div class="col-md-10 col-md-offset-1 col-xs-12 TourText " align="left">
		<div class="col-md-12" align="center">
			<h2>Tour por la Bodega</h2>
		</div>
		<p>
			La reseña histórica, la vendimia y la elaboración, la cava, el fraccionamiento y la degustación, forman parte del tour que se desarrolla en un clima amigable y desestructurado donde los visitantes espontáneamente consultan aquello que les resulta atractivo, así se genera un intercambio absolutamente enriquecedor. Al visitarnos notarán con qué esmero se cuidan los viñedos y se desarrolla la vinificación.
		</p>
		<p> Además frecuentemente Don Alfredo Roca o su hijo Alejandro comparten su afable saludo a los turistas, muestra de la especial dedicación con que la Familia Roca hace su labor y disfruta al compartirla.
		</p><p>
		Vinos con gran personalidad expresan magia y calidez, y dejan siempre abierta la puerta para volver a visitar la cuna de estos caldos selectos.
	</p>
</div>
<div class="clearfix"></div>
<div class="col-md-12 col-xs-12  " align="left">
	<div align="center"><h2>Servicio Atención al Visitante</h2></div>
	<div class="col-md-5 col-xs-12 col-md-offset-1 textoVinedos">
		<p>
			<h3>Visitas guiadas bodega </h3>
			<ul class="visitante">
				<li>Sin reserva previa</li>
				<li>Con cargo </li>
				<li>Incluye degustación simple</li>
				<li>de lunes a viernes hábiles/ horarios:  8,00 - 9,00 - 10,00 - 11,00 - 12,00 - 13,30 -14,30 - 15,30 hs </li>
				<li><b>Duración de la visita:</b>  30 a  40 minutos </li>
				<li><b>Idiomas:</b>  español e inglés</li>
			</ul>
		</p>
		<p>Grupos mayores a 20 personas, por favor avisar previamente para una mejor atención</p>
		<hr class="linetour">
		<p><h3>Visitas educativas: </h3>Consultar programas</p>
	</div>
	<div class="col-md-5 col-xs-12 col-md-offset-1 textoVinedos">
		<p><h3>Degustación dirigida</h3>
			<ul class="visitante">
				<li>Con cargo</li>
				<li>hasta 7 personas</>
					<li>Reserva previa :  0260 4497250 roca@rocawines.com </li>
				</ul>
				<hr class="linetour">
			</p>
			<p><h3>Visita al viñedo</h3>
				<ul class="visitante">
					<li>Con cargo</li>
					<li>Con reserva previa: 0260 4497250 roca@rocawines.com </li>
				</ul>
				<hr class="linetour">
			</p>
			<p><h3>Ventas</h3>
				<ul class="visitante">
					<li>de lunes a viernes hábiles de 7:30 a 16:30 hs </li>
					<li>efectivo y tarjetas</li>
					<li>En Bodega </li>
					<li>Puntos de ventas resto del País <a href="#puntosventa" class="btn btn-roca">Ver Puntos >></a></li>
				</ul>
			</p>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</section>
<section id="TurismoTexto">
	<div class="col-md-2"></div>
	<div class="col-md-8 col-xs-12">
		Vinos con gran personalidad expresan magia y calidez, y dejan siempre abierta la puerta para volver a visitar la cuna de estos caldos selectos.
	</div>
	<div class="clearfix"></div>
</section>
<section class="infotour">
	<div class="col-md-12 " align="center">
		<h2 id="puntosventa">Puntos de Venta</h2>
	</div>
	<div class="col-md-12 col-xs-12  textoVinedos" align="left">
		<!-- <div align="center"><h3>Servicio Atención al Visitante</h3></div> -->
		<div class="col-md-5 col-xs-12 col-md-offset-1">
			<p>
				<h3>Bodega </h3>
				Ruta Prov 165 esq Calle La Pichana – Cañada Seca – San Rafael – Mendoza <br>
				<b>Teléfonos:</b> 0260 4497250/117/194 (rotativas) <br>
				<b>Email:</b> <a href="mailto:nacionales@rocawines.com">nacionales@rocawines.com</a>
			</p>
		</div>
		<div class="col-md-5 col-xs-12 col-md-offset-1">
			<p>
				<h3>Oficina Comercial Bs. As  </h3>
				<b>Teléfonos:</b> 011-4783 4143 .Ciudad Autónoma de Bs. As. <br>
				<b>Email:</b> <a href="mailto:groca@rocawines.com">groca@rocawines.com</a> | <a href="mailto:ventas@rocawines.com">ventas@rocawines.com</a> | <a href="mailto:buenosaires@rocawines.com ">buenosaires@rocawines.com </a>
			</p>
		</div>
		<div class="clearfix"></div>
		
	</div>
	<div class="clearfix"></div>
</section>
<section id="Distinciones">
	<div align="center"><h2>Calidad Túristica</h2></div>
	<div class="col-md-6 col-xs-12"  align="center">
		<div class="col-md-5 col-md-offset-1">	<div id="TA_certificateOfExcellence92" style="margin-top: 30px" class="TA_certificateOfExcellence">
			<ul id="BEvHkd2EJ" class="TA_links KomsIwtdjDJC">
				<li id="JbXKoQjxi3v" class="Mq6zqrHRay">
					<a target="_blank" href="https://www.tripadvisor.com.ar/Attraction_Review-g312782-d3961651-Reviews-Bodega_Alfredo_Roca-San_Rafael_Province_of_Mendoza_Cuyo.html"><img src="https://www.tripadvisor.com.ar/img/cdsi/img2/awards/CoE2016_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a>
				</li>
			</ul>
		</div>
		<script src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=92&amp;locationId=3961651&amp;lang=es_AR&amp;year=2016&amp;display_version=2"></script>
	</div>
	<div class="col-md-6 calidadTrip" align="left">
		Con el Certificado de Excelencia, TripAdvisor reconoce a los negocios de la industria turistica que recibieron, de manera consistente, elogios y altas clasificaciones por parte de los viajeros en el sitio
	</div>
</div>
<div class="col-md-6 col-xs-12 directrices" align="center">
	<img src="{{URL::to('/')}}/images/Directrices-Bodegas.jpg" alt="">
</div>
<div class="clearfix"></div>
<div align="center" style="margin-top:35px"><h2>Membresias</h2></div>
<div class="col-md-12">
	<div class="col-md-4 col-lg-4 col-xl-3 col-xs-12 col-sm-6"><img  style="width: 100%; height: auto" src="{{URL::to('/')}}/images/arg_tierra_vinos.jpg" alt=""></div>
	<div class="col-md-4 col-lg-4 col-xl-3 col-xs-12 col-sm-6"><img style="width: 100%; height: auto" src="{{URL::to('/')}}/images/caminos_vinos.jpg" alt=""></div>
	<div class="col-md-4 col-lg-4 col-xl-3 col-xs-12 col-sm-6"><img style="width: 100%; height: auto" src="{{URL::to('/')}}/images/catedral_vino.jpg" alt=""></div>
	<div class="col-md-4 col-lg-4 col-xl-3 col-xs-12 col-sm-6" style="margin-top:35px"><img style="width: 100%; height: auto" src="{{URL::to('/')}}/images/Great-wine-capitals.jpg" alt=""></div>
	<div class="col-md-4 col-lg-4 col-xl-3 col-xs-12 col-sm-6" style="margin-top:45px"><img style="width: 100%; height: auto" src="{{URL::to('/')}}/images/wineinmoderation.jpg" alt=""></div>
</div>
<div class="clearfix"></div>
</section>
@stop