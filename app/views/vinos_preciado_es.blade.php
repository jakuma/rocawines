@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">
	<h1 class="tituloPagina vinos fadeIn">
		Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
</section>
<section id="nuestrosvinos" class="detalleVinos">
	<div align="center"><h2>Alfredo Roca - Preciado</h2></div>
	<div class="texto col-md-8 col-md-offset-2" align="center">
		<p>
Conoce nuestro suelo, descubre nuestros aromas, admira nuestro paisaje.<p>
	</div>
	<div class="clearfix"></div>
</section>

<section id="losvinos">
	<div class="col-xs-12 col-md-12">
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/preciado/preciado.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Preciado Blend</h3>
				<p>Intenso color en donde se reflejan perfectos tonos rojos violáceos. Predominan aromas de frutos rojos amalgamados con dulces notas de vainilla provenientes de su guarda en roble. En boca posee una gran estructura por su alta concentración de taninos bien maduros. Vino elegante de gran persistencia.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_preciado.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		
		<div class="col-md-6 col-xs-12 ">
		
		</div>
	</div>
	<div class="clearfix"></div>
</section>
@Stop