@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">
	<h1 class="tituloPagina vinos fadeIn">
	Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
</section>
<section id="nuestrosvinos" class="detalleVinos">
	<div align="center"><h2>Alfredo Roca  Cuvée Especial</h2></div>
	<div class="texto col-md-8 col-md-offset-2" align="center">
		<p>
			
Conoce nuestro suelo, descubre nuestros aromas, admira nuestro paisaje.<p>
			</div>
			<div class="clearfix"></div>
		</section>
		<section id="losvinos">
			<div class="col-xs-12 col-md-12">
				<div class="col-md-6 col-xs-12 itemvino">
					<div class="imgVino col-md-5">
						<img src="{{URL::to('/')}}/images/vinos/cuvee/cuvee-especial.jpg" alt="">
					</div>
					<div class="textovino col-md-7">
						<h3>Cuvée especial </h3>
						<p>Color acerado vivaz, de pequeñas burbujas con muy buena persistencia.</p>
						<p>En nariz posee notas a tostadas aportadas por las levaduras que se ensamblan finamente con recuerdos a flores blancas y durazno. En boca es fresco y persistente con una acidez elegante que da final limpio y delicado.</p>
						<div class="fichapdf">
							<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_brut_nature.pdf" class="btn btn-roca">
								Ficha Técnica
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xs-12 ">
					
				</div>
		
			</div>
			<div class="clearfix"></div>
		</section>
		@Stop