@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/laBodega2.jpg" alt="">
	<h1 class="tituloPagina bodega fadeIn">
	La Bodega
	</h1>
	<div class="textosup textobod fadeIn ">
		Alfredo Roca, enólogo, en el año 1976 concretó su sueño del establecimiento propio para la elaboración de las uvas provenientes de los viñedos de la familia.
	</div>
</section>
<section id="historia">
	<div class="col-md-12" align="center">
		<h2>Nuestra Historia</h2>
	</div>
	<div class="col-md-6 col-xs-12  " align="center">
		<div class="borderImg">
			<img src="{{ URL::to('/') }}/images/internas/historia.jpg" class="historia" alt="">
		</div>
	</div>
	
	<div class="col-md-5 col-sm-10 col-xs-12  textoBodega" align="left">
		<p>Nuestros antepasados llegaron desde España e Italia en el año 1912 y dire	ctamente se afincaron en San Rafael y se dedicaron a lo que ellos sabían hacer, cultivar viiñedos. Casualmente lo que en su mayoría plantaron fue Malbec, sin duda conocían que se adaptaba a la región. Ellos me supieron transmitir el amor por el trabajo de la tierra y el esfuerzo necesario para progresar. Por mi parte siempre he buscado transmitir a mis hijos, que ya están en la dirigencia, estos valores.</p>
		<p>
			La bodega se construyó originalmente en 1905 y la estructura principal a sido mantenida hasta la actualidad con la incorporación y adaptación a las nuevas tecnologías, mantiene la arquitectura original de dos naves, con una tercera que se sumó al mismo estilo
		</p>
	</div>
	<div class="clearfix"></div>
</section>
<section id="bodegaTexto">
	
		<div class="col-md-2"></div>
		<div class="col-md-8 col-xs-12">
			<span class="quote"> " </span> Bodega Roca es la concreción de mi gran sueño, como enólogo que soy, el establecimiento propio para la elaboración de las uvas provenientes de los viñedos de la familia ha sido un gran tesoro. Somos una empresa 100% familiar con viñedos en San Rafael por cuatro generaciones <span class="quote"> " </span>
		</div>
		<div class="col-md-2"></div>
		<div class="firma col-md-10 col-xs-12" >
			Alfredo Roca
		</div>
		<div class="col-md-2"></div>
	<div class="clearfix"></div>
</section>
<section id="historia">
	<div class="col-md-12" align="center">
		<h2>La Bodega</h2>
	</div>
	
		<div class="col-md-5 col-xs-12 internasFotos" align="center">
			<div class="borderImg">
				<img src="{{ URL::to('/') }}/images/bodegaFrente.jpg" class="historia" alt="">
			</div>
			
			<div class="borderImg">
				<img src="{{ URL::to('/') }}/images/bodegaFrente2.jpg" class="historia" alt="">
			</div>
		</div>
		
		<div class="col-md-6 col-sm-10 col-xs-12  textoBodega" align="left">
			<p>Situada en las afueras de San Rafael, rodeada de viñedos y alamedas. La belleza del paisaje cercano a la bodega agrada por sus límpidos cielos, sus aguas claras y transparentes propias del deshielo de la cordillera de Los Andes. Es allí, en San Rafael, sur de la provincia de Mendoza, donde la familia Roca se ha dedicado al cuidado de sus viñedos por cuatro generaciones. Alfredo Roca, enólogo, en el año 1976 concretó su sueño del establecimiento propio para la elaboración de las uvas provenientes de los viñedos de la familia. Actualmente sus hijos forman parte de la dirigencia de la empresa, la que ha sabido adaptarse al mercado actual con tecnología de punta logrando posicionarse como referente de la región.</p>
			<p>
				
				El lema de la empresa: “Dedicación familiar, vinos con alma” sintetiza los valores y el compromiso de la misma. El prestigio de sus vinos es resultado del esfuerzo constante en obtener una excelente materia prima, realizar una adecuada cosecha de la uva y una muy buena elaboración y guarda del vino. La empresa ha logrado los estándares exigidos a nivel internacional en cada rubro lo que queda demostrado por sus exportaciones que incluyen mercados exigentes de Europa y Estados Unidos entre otros. El Sistema de Gestión de la Calidad está certificado bajo las normas ISO 9001-2008.
			</p>
			<p>
				Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
			</p>
		</div>
	
	<div class="clearfix"></div>
</section>
<section id="calidad">
	<div class="col-md-12" align="center">
		<h2>Calidad</h2>
	</div>
	<div class="col-md-1 col-md-offset-2"><img src="{{URL::to('/')}}/images/calidad/iram.jpg" alt=""> </div>
	<div class="col-md-3 pdfCalidad"><p><a href="{{URL::to('/')}}/pdf/calidad/iram_bpm.pdf">Certificado Norma IRAM BPM</a></p><p><a href="{{URL::to('/')}}/pdf/calidad/iram_iso.pdf">Certificado Norma IRAM ISO 9001</a></p></div>
	<div class="col-md-1 col-md-offset-2"><img src="{{URL::to('/')}}/images/calidad/iqnet.jpg" alt=""></div>
	<div class="col-md-3 pdfCalidad">
		<p><a href="{{URL::to('/')}}/pdf/calidad/iqnet_iso.pdf">	Certificado Norma IQNET ISO 9001</a></p>
	</div>
	<div class="clearfix"></div>
</section>
<section id="bodegaFotos">
	<!-- <div align="center"><h2>Galeria de Fotos</h2></div> -->
	<div id="lightgallery">
		<div class="col-md-4">
			<a href="{{ URL::to('/') }}/images/fotos/embotellado_bodega_roca.jpg" class="foto">
				<img src="{{ URL::to('/') }}/images/fotos/embotellado_bodega_roca_C.jpg" />
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{ URL::to('/') }}/images/fotos/frente_bodega_roca.jpg" class="foto">
				<img src="{{ URL::to('/') }}/images/fotos/frente_bodega_roca_C.jpg" />
			</a>
		</div>
		<div class="col-md-4">
			<a href="{{ URL::to('/') }}/images/fotos/sala_arte.jpg" class="foto">
				<img src="{{ URL::to('/') }}/images/fotos/sala_arte_C.jpg" />
			</a>
		</div>
		
		<div class="clearfix"></div>
		...
	</div>
</section>
@stop