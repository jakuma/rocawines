@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<h1 class="tituloPagina vinos fadeIn">
		Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">
</section>
<section id="nuestrosvinos" class="detalleVinos">
	<div align="center"><h2>Alfredo Roca Reserva de Familia</h2></div>
	<div class="texto col-md-8 col-md-offset-2" align="center">
		<p>
Cada vid recibe nuestro cuidado, cada vino nuestra custodia. Cada botella expresa la dedicación, y la pasión por el arte de hacer vinos.<p>
	</div>
	<div class="clearfix"></div>
</section>

<section id="losvinos">
	<div class="col-xs-12 col-md-12">
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/reserva/reserva-familia-malbec.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Reserva de Familia Malbec</h3>
				<p>Intenso color en donde se reflejan perfectos tonos rojos violáceos. Predominan aromas de frutos rojos amalgamados con dulces notas de vainilla provenientes de su guarda en roble. En boca posee una gran estructura por su alta concentración de taninos bien maduros. Vino elegante de gran persistencia en boca.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_reserva_malbec.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/reserva/reserva-familia-tempranillo.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Reserva de Familia Tempranillo</h3>
				<p>Vino de color rojo profundo, que ofrece gran complejidad de aromas entre los que se destacan las notas de mermeladas de frutos rojos, vainilla y tabaco. Tiene la virtud de estar muy bien balanceado en boca y a su vez poseer gran cuerpo, las notas aportadas por el roble aparecen muy bien integradas al vino. Gran potencial en la guarda.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_reserva_tempranillo.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
			<div class="col-md-6 col-xs-12 itemvino">
			<div class="imgVino col-md-5">
				<img src="{{URL::to('/')}}/images/vinos/reserva/reserva-familia-pinot-noir.jpg" alt="">
			</div>
			<div class="textovino col-md-7">
				<h3>Reserva de Familia Pinot Noir</h3>
				<p>Color rojo de gran vivacidad demostrado en su tono rubí, intenso y profundo. En nariz es sumamente elegante en interesante gracias a su especiada paleta de aromas. En boca es complejo y delicado, se destacan sus sabores de frutos oscuros y especias agraciados por su maduración en roble francés.</p>
				<div class="fichapdf">
					<a target="_blank" href="{{URL::to('/')}}/pdf/fichas_enologicas_reserva_pinot.pdf" class="btn btn-roca">
						Ficha Técnica
					</a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 ">
		
		</div>
	</div>
</section>
