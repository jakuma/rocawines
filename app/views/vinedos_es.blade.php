@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinedos.jpg" alt="">
	<h1 class="tituloPagina vinedos fadeIn">
	Viñedos
	</h1>
	<div class="textosup textovinedo fadeIn">
		Nuestros viñedos se encuentran ubicados en San Rafael, oasis al sur de la provincia de Mendoza, corazón vitivinícola de Argentina.
	</div>
</section>
<section id="vinedos">
	<div class="col-md-12" align="center">
		<h2>Nuestros Viñedos</h2>
	</div>
	
	
	<div class="col-md-5 col-sm-10 col-sm-offset-1  col-md-offset-1 col-xs-12  textoVinedos" align="left">
		<p>Situados al pie de la Cordillera de Los Andes, estos son irrigados con agua de deshielo del Río Diamante. Una baja cantidad de lluvias, intenso sol y noches frescas son características de nuestro clima, lo que permite una lenta maduración de las uvas.</p>
		<p>
			Estas condiciones que diferenciarán nuestros vinos, se ven reflejados en los intensos colores, en la gran expresión de aromas y complejidad de sabores.
		</p>
		<p>
			En total nuestros viñedos forman 114 has. que se reparten en tres propiedades: Finca La Perseverancia; Finca Los Amigos y Finca Santa Herminia.
		</p>
	</div>
	<div class="col-md-5 col-sm-10 col-sm-offset-1 col-xs-12  textoVinedos" align="left">
		<p>
		Se encuentran ubicadas a 800 m. sobre el nivel del mar, poseen un régimen de lluvias de 200mm anuales lo que hace que posean un promedio de humedad relativa de 50%, favoreciendo la sanidad de las uvas y una completa maduración de las mismas. Las variedades que cultivamos son: Malbec, Cabernet Sauvignon, Syrah, Merlot, Pinot Noir, Tempranillo, Bonarda y Sangiovese entre las tintas y Chardonnay, Chenin, Sauvignon Blanc y Tocai (o Sauvignonasse) entre las blancas.</p>
		
	</div>
	<div class="clearfix"></div>
	
</section>
<section id="fincas">
	<div class="col-md-6 nopadding">
		<img src="{{ URL::to('/') }}/images/internas/finca1.jpg" alt="">
	</div>
	<div class="col-md-6 textos">
		<h2>Finca Los Amigos</h2>
		<p>El viñedo se encuenta al noreste del departamento de San Rafael, distrito Cuadro Nacional. Cuenta con una superficie plantada de 10 ha. Es un viñedo de buena edad, se destacan Bonarda de 40 años, Malbec y Cabernet de 60 años.</p>
		<div class="col-md-12 col-xs-12" align="center">
			<button class="modalinfo btn btn-roca" rel="losamigos"><i class="fa fa-edit"></i> Ampliar  </button>
		</div>
	<!-- 	<div class="col-md-6 col-xs-6" align="center">
			<button class="modalinfo btn btn-roca" rel="losamigos"><i class="fa fa-photo"></i> Fotos  </button>
		</div> -->
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	<div class="col-md-6 col-xs-12 hidden-md visible-sm visible-xs nopadding">
		<img src="{{ URL::to('/') }}/images/internas/finca2.jpg" alt="">
	</div>
	<div class="col-md-6 col-xs-12 textos">
		<h2>Finca Santa Herminia</h2>
		<p>Ubicada al sudeste de la ciudad de San Rafael, distrito Ciudad. El viñedo cuenta con 44 has, con una edad promedio de 15 años. Los varietales plantados son Cabernet Sauvignon, Pinot Noir, Malbec, Merlot, Chardonnay </p>
		<div class="col-md-12 col-xs-12" align="center">
			<button class="modalinfo btn btn-roca" rel="SantaHerminia"><i class="fa fa-edit"></i> Ampliar  </button>
		</div>
<!-- 		<div class="col-md-6 col-xs-6" align="center">
			<button class="modalinfo btn btn-roca" rel="SantaHerminia"><i class="fa fa-photo"></i> Fotos  </button>
		</div> -->
		<div class="clearfix"></div>
	</div>
<div class="col-md-6 hidden-sm hidden-xs col-cs-12  nopadding">
		<img src="{{ URL::to('/') }}/images/internas/finca2.jpg" alt="">
	</div>
	<div class="clearfix"></div>
	
		<div class="col-md-6 col-xs-12 nopadding">
		<img src="{{ URL::to('/') }}/images/internas/Finca-La-Perseverancia.jpg"  alt="">
	</div>
	<div class="col-md-6 textos">
		<h2>Finca La Perseverancia</h2>
		<p>Viñedo ubicado al Noreste del departamento de San Rafael, distrito Cuadro Nacional. Con un total de 55 has plantadas, 55% de ellas ha sido plantado durante la década del 40 y 50. En los últimos 10 años, se han cultivado nuevos viñedos que están en muy buena producción.</p>
		<div class="col-md-12 col-xs-12" align="center">
			<button class="modalinfo btn btn-roca" rel="laperseverancia"><i class="fa fa-edit"></i> Ampliar  </button>
		</div>
		<!-- <div class="col-md-6 col-xs-6" align="center">
			<button class=" btn btn-roca fotos" rel="galeria3"><i class="fa fa-photo " ></i> Fotos  </button>
		</div> -->
		
	</div>
	<div class="clearfix"></div>
</section>
<section id="terroir">
	<div class="col-md-12" align="center">
		<h2>Terroir</h2>
	</div>
	
	<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12  texto-terroir" align="center">
		<p></p>
		<p>
			Dominada al Oeste por la Cordillera de los Andes, al Norte por una zona desértica, al Este por la llanura y al Sur por la Payunia malargüina.
		</p>
		<button class="modalinfo btn btn-roca-light" rel="terroirmodal"><i class="fa fa-edit"></i> Ampliar  </button>
	</div>
	<div class="clearfix"></div>
</section>
<!-- Modal -->
<div id="losamigos" class="modal fade modalvinedos" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Finca Los Amigos</h4>
				
			</div>
			<div class="modal-body">
				<b>Ubicación:</b> Noreste del departamento de San Rafael, distrito Cuadro Nacional
				Superficie: 55 has.
				<br>
				<b>Suelo:</b> Franco a Franco Arenoso de gran profundidad con muy buen drenaje.
				<br>
				<b>Altitud:</b> 700 msnm
				<br>
				<b>Condiciones particulares:</b> el viñedo se encuentra al límite de la zona cultivada lindando con el desierto mendocino. Es por esto que las condiciones son representativas de este tipo de climas con abundante sol durante el día siempre acompañado de una permanente briza y noches bien frescas.
				<br>
				<b>Período libre de heladas:</b> de 180 a 190 días (del 21 – 30 de octubre al 20-25 de abril)
				<br>
				<b>Precipitación anual promedio:</b> 260 mm
				<br>
				<b>Plantaciones:</b> Dicho viñedo se caracteriza por poseer plantaciones de buena edad, 55% de ellas ha sido plantado durante la década del 40 y 50. En los últimos 10 años, se han cultivado nuevos viñedos que están en muy buena producción.
				<br>
				<b>Variedades:</b> Cabernet, Malbec, Syrah, Sangiovese, Tempranillo. Chardonnay, Sauvignon Blanc, Chenin.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- FINCA 3 -->
<!-- Modal -->
<div id="laperseverancia" class="modal fade modalvinedos" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Finca La Perseverancia</h4>
				
			</div>
			<div class="modal-body">
				<b>Ubicación:</b> Noreste del departamento de San Rafael, distrito Cuadro Nacional
				Superficie: 55 has.
				+7
				<br>
				<b>Suelo:</b> Franco a Franco Arenoso de gran profundidad con muy buen drenaje.
				<br>
				<b>Altitud:</b> 700 msnm
				<br>
				<b>Condiciones particulares:</b> el viñedo se encuentra al límite de la zona cultivada lindando con el desierto mendocino. Es por esto que las condiciones son representativas de este tipo de climas con abundante sol durante el día siempre acompañado de una permanente briza y noches bien frescas.
				<br>
				<b>Período libre de heladas:</b> de 180 a 190 días (del 21 – 30 de octubre al 20-25 de abril)
				<br>
				<b>Precipitación anual promedio:</b> 260 mm
				<br>
				<b>Plantaciones:</b> Dicho viñedo se caracteriza por poseer plantaciones de buena edad, 55% de ellas ha sido plantado durante la década del 40 y 50. En los últimos 10 años, se han cultivado nuevos viñedos que están en muy buena producción.
				<br>
				<b>Variedades:</b> Cabernet, Malbec, Syrah, Sangiovese, Tempranillo. Chardonnay, Sauvignon Blanc, Chenin.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div id="SantaHerminia" class="modal fade  modalvinedos" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Finca Santa Herminia</h4>
				
			</div>
			<div class="modal-body">
				<b>Ubicación:</b> sudeste de la ciudad de San Rafael, distrito Ciudad.
				<br>
				<b>Superficie:</b> 44 has.
				<br>
				<b>Suelo:</b> limo-arcilloso. Capas de ripio y piedras cercanas a la superficie
				<br>
				<b>Altitud:</b> 750 msnm
				<br>
				<b>Condiciones:</b> dicho viñedo se encuentra cercano al Río Diamante y a la ciudad de San Rafael. Esto le da suelos de mayor drenaje por las cercanías al río e inviernos no tan crudos por la cercanía a la ciudad, lo que disminuye el riesgo de heladas tardías.
				<br>
				<b>Período libre de heladas:</b> de 180 a 190 días (del 01 – 10 de octubre al 26 – 30 de abril)
				<br>
				<b>Precipitación anual promedio:</b> 300 mm
				<br>
				<b>Plantaciones:</b> la mayoría de este viñedo posee una edad promedio de 15 años
				<br>
				<b>Variedades:</b> Cabernet Sauvignon, Pinot Noir, Malbec, Merlot, Chardonnay.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div id="terroirmodal" class="modal fade modalvinedos" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4>Nuestro Terroir</h4>
				
			</div>
			<div class="modal-body">
				<p><b>Ubicación:</b>  en la región sur de la provincia de Mendoza en el centro-oeste de la República Argentina, entre los 32º y 38º de latitud sur y los 66º 30′ y los 70º 30′ de longitud oeste.
				</p>
				<p>
					Dominada al Oeste por la Cordillera de los Andes, al Norte por una zona desértica, al Este por la llanura y al Sur por la Payunia malargüina.
				</p>
				<p>
					<b>Clima y precipitaciones:</b> El clima es continental, semiárido, con una influencia marcada de la Cordillera de los Andes. Las precipitaciones apenas superan los 250 mm anuales.
				</p>
				<p>
					En el llano el clima es templado continental semiseco con lluvias en verano, pero a medida que se asciende hacia la montaña, las temperaturas disminuyen acompañadas de nieve cuya caída se intensifica de mayo a septiembre. San Rafael por su ubicación está influenciada por cordones cordilleranos, de menor altura, lo que provoca una variación térmica grande entre el día y la noche. El verano (de diciembre a marzo) es caluroso, con eventuales tormentas y su temperatura oscila entre los 15º C y los 36° C; el otoño (de marzo a junio), es sereno con descenso de la temperatura y días mas cortos, el invierno (de julio a septiembre) es frío y seco, el termómetro oscila entre los 0º C y los 10º C y en alta montaña las nevadas pueden alcanzar hasta los 600 mm; la primavera (de septiembre a diciembre) trae días cálidos, el renacer de la naturaleza.
				</p>
				<p>
					Con la llegada de los primeros calores y el consiguiente deshielo de la nieve en cordillera, se incrementa el cauce de lagunas y ríos que riegan los extensos cultivos.
				</p>
				<p>
					El agua de gran calidad, se va cargando de minerales a medida que avanza hacia el llano. Los cultivos de este departamento están regados casi en su totalidad por agua de deshielo, y en pocos casos con agua  subterránea, también de buena calidad.
				</p>
				<p>
				<b>Suelos:</b> San Rafael tiene como característica una amplia variedad en textura de suelos. La textura contiene materiales originarios provenientes de la erosión de las rocas cordilleranas, que no han sufrido modificaciones en el sitio donde fueron depositados, luego de ser transportados por distintos agentes como eólico (viento), coluvial (gravedad) y aluvional, esto es con el avance de los ríos y arroyos que la surcan y que nacen en la cordillera y corren hacia el este. Así están definidos los suelos gravosos en el pedemonte, de una profundidad limitada por material pétreo, llegando a arenosos y bien profundos hacia el este. Las características propias de cada tipo de suelo responden, en gran parte, a las características mineralógicas que heredaron de las rocas originarias, pero también es importante la influencia de otros factores de formación como el clima, los organismos (microorganismos, plantas, animales y hombre), el relieve y el tiempo transcurrido, que tuvieron sobre los materiales iniciales.</p>
				<p>En general son suelos de bajo contenido de materia orgánica y de buena permeabilidad. Es muy grande la variabilidad de textura en distancias cortas y es fácil encontrar, a pocos metros de una tierra de perfil totalmente arenoso, otra en la cual se observan capas limo – arcillosas. Estas variaciones constituyen muchas veces la clave de la heterogeneidad en el estado de un cultivo.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop