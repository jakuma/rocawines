@extends ('templates/inicio')
@section ('content')
  <div id="inicio">
    <img src="{{ URL::to('/') }}/images/tonel.jpg" alt="">
    <div id="box_inicio">
    	<div align="center">
    		<img src="./images/logo_blanco_vertical.png" alt="" class="logoR">
    	</div>
    	<div class="pregunta">
    		¿ Sos mayor de 18 años ?
    	</div>
    	<div align="center" style="margin-bottom:20px">
    		<div class="botones">SI</div>
    		<div class="botones">NO</div>
    	</div>
    	<div id="recuerda" align="center">
    		<div class=""><input type="checkbox">
    			Recuérdame la próxima vez
    		</div>
    	</div>

    </div>

  </div>