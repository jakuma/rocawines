@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/vinos.jpg" alt="">

	<h1 class="tituloPagina vinos fadeIn">
		Vinos
	</h1>
	<div class="textosup textovino fadeIn ">
		Familia Roca posee una gran pasión: lograr las mejores uvas y criar excelentes vinos.
	</div>
</section>
<section id="nuestrosvinos">
	<div align="center"><h2>Nuestros Vinos</h2></div>
	<div class="texto col-md-8 col-md-offset-2">
		<p>Nuestra Familia nació como productores de uvas, siempre en contacto con la tierra y los viñedos, buscando las mejores condiciones para las vides, entendiendo y leyendo lo que el clima y los suelos transmiten a nuestras uvas. A través de los años esto ha sido fundamental para conseguir grandes vinos. Comenzamos en el viñedo buscando el mejor desarrollo de aromas, colores y sabores de nuestras uvas para luego transmitirlas a cada vino en particular.<p>
<p>
En la familia amamos los grandes vinos, los vinos que muestran elegancia y estilo, que saben transmitir la esencia de su tierra y de la historia que el hombre ha marcado sobre ellos, este sentimiento se ha transformado en nuestra filosofía que aplicamos día a día en cada vino.
</p>
	</div>
	<div class="clearfix"></div>
</section>

<section id="vinosList" align="center">
	<div class="cont_vinos ">
		<div class="cover_vino_full animate_hover">
			<div class="tableVino">
					<div class="info_vino grad-hoeiz-black" align="left">
						<div class="col-md-5 col-md-offset-1">
						<h2 >Cien Cosechas</h2>
						
						<p style="margin-top: 20px; font-size: 16px">
							Celebramos las 100 cosechas de nuestra Familia abocada enteramente a los viñedos. La vendimia 2012 marcó los 100 años de la llegada a San Rafael de nuestros antepasados.  
						</p>
						<a href="{{URL::to('/')}}/pdf/ficha_enologica_cien_cosechas.pdf" style="margin: 20px 0 " class="btn btn-roca-light">
							Ficha Técnica
						</a>
						</div>
						<!-- <a href="" class="cuadro"><span class="border"></span></a>  -->
					</div>

					<div class="cover_background animated_bg"  style="background-image:url({{URL::to('/')}}/images/vinos/cien_cosecha.jpg)"></div>
			</div>
		</div>
		<div class="cover_vino animate_hover" >
			<div class="tableVino">
				<div class="info_vino">
				<img src="{{URL::to('/')}}/images/logo_roca_solo.png" class="logo" alt="">

				<h2>Alfredo Roca Fincas</h2>
				<p>Hay un placer que nos convoca día a día a nuestros viñedos.</p>
					<a href="{{URL::to('/')}}/es/vinos/fincas" class="cuadro"><span class="border"></span></a> 
				</div>
				<div class="cover_background animated_bg"  style="background-image:url({{URL::to('/');}}/images/vinos/linea-finca.jpg)"></div>
			</div>
		</div>
		<div class="cover_vino animate_hover">
			<div class="tableVino">
				<div class="info_vino">
				<img src="{{URL::to('/')}}/images/logo_roca_solo.png" class="logo" alt="">
				<h2>Roca</h2>
				<p>Conoce nuestro suelo, descubre nuestros aromas, admira nuestro paisaje.</p>
				<a href="{{URL::to('/')}}/es/vinos/roca" class="cuadro"><span class="border"></span></a> 

				</div>
				<div class="cover_background animated_bg" style="background-image:url({{URL::to('/');}}/images/vinos/linea-roca.jpg)"></div>
			</div>
		</div>
		<div class="cover_vino animate_hover" >
			<div class="tableVino">
			
				<div class="info_vino">
				<img src="{{URL::to('/')}}/images/logo_roca_solo.png" class="logo" alt="">
				<h2>Reserva de Familia</h2>
				<p>Hay un placer que nos convoca día a día a nuestros viñedos.</p>
					<a href="{{URL::to('/')}}/es/vinos/reserva" class="cuadro"><span class="border"></span></a> 
				</div>
				<div class="cover_background animated_bg"  style="background-image:url({{URL::to('/');}}/images/vinos/reserva_familia.jpg)"></div>
			</div>
		</div>
		<div class="cover_vino animate_hover">
			<div class="tableVino">
				<div class="info_vino">
				<img src="{{URL::to('/')}}/images/logo_roca_solo.png" class="logo" alt="">
				<h2>Dedicacion Personal</h2>
				<p>Conoce nuestro suelo, descubre nuestros aromas, admira nuestro paisaje.</p>
				<a href="{{URL::to('/')}}/es/vinos/roca" class="cuadro"><span class="border"></span></a> 

				</div>
				<div class="cover_background animated_bg" style="background-image:url({{URL::to('/');}}/images/vinos/dedicacion_personal.jpg)"></div>
			</div>
		</div>
		<div class="cover_vino animate_hover">
			<div class="tableVino">
				<div class="info_vino">
				<img src="{{URL::to('/')}}/images/logo_roca_solo.png" class="logo" alt="">
				<h2>Dedicacion Personal</h2>
				<p>Conoce nuestro suelo, descubre nuestros aromas, admira nuestro paisaje.</p>
				<a href="{{URL::to('/')}}/es/vinos/roca" class="cuadro"><span class="border"></span></a> 

				</div>
				<div class="cover_background animated_bg" style="background-image:url({{URL::to('/');}}/images/vinos/curvee-especial.jpg)"></div>
			</div>
		</div>
		<div class="cover_vino animate_hover">
			<div class="tableVino">
				<div class="info_vino">
				

				</div>
				
			</div>
		</div>

	</div>
</section>