@extends ('templates/layaout')
@section ('content')
<section id="cont-internas">
	<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/bg_botellas.jpg" alt="">
	<h1 class="tituloPagina bodega fadeIn">
	Contáctenos
	</h1>
	<div class="textosup textobod fadeIn ">
		Envienos su consulta.
	</div>
</section>
<section id="contacto" class="clearfix">
	<div class="col-md-12" align="center">
		<h2>Contáctenos</h2>
	</div>
	<div class="col-md-5 col-md-offset-1 datos" >
		<h3>Datos de contacto</h3>
		<p> <i class="fa fa-phone"></i> <b>Tel (Fax):</b>  54 260  4497194 / 4497117 / 4497250 </p>
		<p><i class="fa fa-envelope "></i> <b>Institucional, Administración y Turismo:</b> roca@rocawines.com</p>
		<p><i class="fa fa-envelope "></i> <b>Ventas Bodega:</b> nacionales@rocawines.com</p>
		<p><i class="fa fa-envelope "></i> <b>Ventas Mercado Interno:</b> groca@rocawines.com</p>
		<p><i class="fa fa-envelope "></i> <b>Exportaciones:</b> export@rocawines.com</p>
		<p><i class="fa fa-envelope "></i> <b>Servicio Atención Clientes:</b> calidad@rocawines.com</p>
	</div>
	<div class="col-md-5 formC">
		<form class="form-horizontal">
			<h3>Envienos una Consulta</h3>
			<!-- Text input-->
			<div id="enviado" class="alert alert-roca"></div>
			<div class="col-md-12">
				<label for="aquien" class="control-label">¿A quien va su consulta?</label>
				<select name="aquien" id="aquien" class="form-control">
					<option value="roca@rocawines.com">Institucional, Administración y  Turismo</option>
					<option value="nacionales@rocawines.com">Ventas Bodega</option>
					<option value="groca@rocawines.com">Ventas Mercado Interno</option>
					<option value="export@rocawines.com">Exportaciones</option>
					<option value="calidad@rocawines.com">Servicio Atención Clientes</option>
				</select>
			</div>
			<div class="col-md-6">
				<label class="control-label" for="nombre">Nombre</label>
				<div class="">
					<input id="nombre" name="nombre" type="text" placeholder="Su nombre Aqui" class="form-control input-md" required>
				</div>
			</div>
			<!-- Text input-->
			<div class="col-md-6">
				<label class=" control-label" for="apellido">Apellido</label>
				<div class="">
					<input id="apellido" name="apellido" type="text" placeholder="su apellido" class="form-control input-md" required>
				</div>
			</div>
			<!-- Text input-->
			<div class="col-md-6">
				<label class=" control-label" for="email">email</label>
				<div class="">
					<input id="email" name="email" type="text" placeholder="Su Email" class="form-control input-md" required>
				</div>
			</div>
			<!-- Text input-->
			<div class="col-md-6">
				<label class=" control-label" for="telefono">Cod Area + Teléfono</label>
				<div class="">
					<div class="col-md-4">
					<input id="area" name="area" type="text" placeholder="area" class="form-control input-md" required></div>
					<div class="col-md-8">
					<input id="telefono" name="telefono" type="text" placeholder="Su Teléfono" class="form-control input-md" required></div>
				</div>
			</div>
			<div class="col-md-12">
				<label class=" control-label" for="consulta">Consulta</label>
				<textarea name="consulta" id="consulta" style="width:100%" rows="3"></textarea>
			</div>
			<div class="col-md-12 " >
				<a href="#" class="btn btn-roca pull-right" id="enviar" >
					Enviar Consulta ->
				</a>
			</div>
		</form>
	</div>
</section>
<section id="ubiContacto" style="margin-bottom: 0px !important" class="clearfix">
	<div class="col-md-12" align="center" >
		<h2>Nuestra Ubicación</h2>
	</div>
	<div class="col-md-10 col-md-offset-1">
		<div id="map">
		</div>
	</div>
</section>
<section class="infotour" style="background-color: #FFF !important">
<div class="col-md-12" align="center" >
		<h2>Vinos Roca en el País</h2>
	</div>
	<div class="col-md-12 col-xs-12  textoVinedos"   align="left">
	
		<div class="col-md-10 col-xs-12 col-md-offset-1">
			
			<div class="col-md-6 col-xs-12">
				<ul class="visitante">
					<li>
						<b>COORDINACIÓN GENERAL, C.A.B.A. y BUENOS AIRES</b>
						<br>Graciela Roca <br>011-4783 4143 <br><a href="mailto:groca@rocawines.com">groca@rocawines.com </a>
					</li>
					<li>
						<b>SAN RAFAEL, GRAL ALVEAR Y MALARGÜE: </b><br>
						Marcos R. Páez <br>0260 154533168 <br><a href="mailto:marcospaez6@hotmail.com">marcospaez6@hotmail.com  </a>
					</li>
					<li>
						<b>MENDOZA: </b><br>
						Federico Sigampa  <br>0261-6394520 <br><a href="mailto:fede13pag@hotmail.com ">fede13pag@hotmail.com</a>
					</li>
					<li>
						<b>PROV. DE RÍO NEGRO, Y CDAD. SAN MARTIN DE LOS ANDES, VILLA LA ANGOSTURA </b><br>
						Aníbal Muñoz  <br>02944 154681892/3 – 02944 - 459215
						<br><a href="mailto:pentalfa2001@yahoo.com.ar">pentalfa2001@yahoo.com.ar   </a><br>
						<a href="mailto:bariloche@rocawines.com">bariloche@rocawines.com   </a>
					</li>
					<!-- <li>
						<b>PUERTO MADRYN </b><br>
						Miguel Angel Calderón<br>
						0280-154574783
						<br><a href="mailto:calderonmiguelangel@yahoo.com.ar">calderonmiguelangel@yahoo.com.ar   </a><br>
					</li> -->
					<li>
						<b>CÓRDOBA: </b><br>
						Cava de Alcalá Distribuidora <br>
						0351- 4812578 / 155726021 /156379728
						<br><a href="mailto:cavadealcala@gmail.com">cavadealcala@gmail.com</a>
					</li>
			
					<li>
						<b>LA PLATA: </b><br>
						Centennial Wines La Plata - Sergio Vara <br>
						0221-155628588
						<br><a href="mailto:vara@fibertel.com.ar ">vara@fibertel.com.ar </a>
					</li>
					<li>
						<b>NEUQUÉN:</b>
						<br> Gastón Sánchez<br> 0260 154483213 / 422809 <br> <a href="mailto:reca1385@yahoo.com.ar">reca1385@yahoo.com.ar </a>
					</li>
				</ul>
			</div>
			<div class="col-md-6 col-xs-12">
				<ul class="visitante">
					<li>
						<b>ROSARIO: </b><br>
						Leonardo Gattelet <br>0341 154022078 <br><a href="mailto:leonardogattelet@express.com.ar">leonardogattelet@express.com.ar  </a>
					</li>
					<li>
						<b>SAN LUIS:</b><br>
						Juan Manuel Boschi <br>0294 154369911
						<a href="mailto:nehuen31@hotmail.com">nehuen31@hotmail.com    </a><
					</li>
					<li>
						<b>SANTA CRUZ Y CALAFATE: </b><br>
						Distribuidora Don Aldo - Darío Hernando <br>
						02966-421251 <br>
						<a href="mailto:distribuidoradonaldo@cotecal.com.ar">distribuidoradonaldo@cotecal.com.ar </a><br>
						<a href="mailto:dariohernando@speedy.com.ar">dariohernando@speedy.com.ar </a>
						<br>
						Vinoteca: 0362 4427747/4444163 <br>
							<a href="mailto:osvaldodelafuente@dinpro.com.ar">osvaldodelafuente@dinpro.com.ar </a>
						     

					</li>
					<li>
						<b>RAFAELA (Prov. de SANTA FE): </b><br>
Jorge Colombo <br> 	03492-695019<br> <a href="mailto:jorge@avizora.com ">jorge@avizora.com </a>
					</li>
					<li>
						<b>TUCUMÁN: </b><br>
						Juan Carlos López Torres<br> 0381 4345041<br> <a href="mailto:jclopeztorres51@gmail.com">jclopeztorres51@gmail.com</a>
					</li>
					<li>
						<b>CHACO, FORMOSA y CORRIENTES:</b><br> J.D.&C. Distribuciones S.A. <br>0362 4468232 / 154601024
						<a href="mailto:osvaldodelafuente@dinpro.com.ar">osvaldodelafuente@dinpro.com.ar</a>
					</li>
					<li>
						<b>USUAHIA  (TIERRA DEL FUEGO): </b>
						<br> Petrel Distribuciones - Esteban Vercen.
						<br>Tel- 2901-412250
						<br><a href="mailto:petreldistribuciones@gmail.com">petreldistribuciones@gmail.com</a>
						<br><a href="https://www.facebook.com/petreldistribuciones/">facebook.com/petreldistribuciones</a>

					</li>
				</ul>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</section>
<section id="rocaMundo" class="clearfix" style="padding-top:30px;background-color: #eee !important">
	<div class="col-md-12" align="center" >
		<h2>Vinos Roca en el Mundo</h2>
	</div>
	<div class="col-md-10 col-md-offset-1">
		<div id="mapM">
		</div>
	</div>
</section>
@stop