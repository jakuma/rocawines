@extends ('templates/layaout')
@section ('content')

<section id="cont-internas">
<img class="fade-in one" src="{{ URL::to('/') }}/images/internas/familia2.jpg" alt="">
	<h1 class="tituloPagina familia fadeIn">
	La Familia
	</h1>
	<div class="textosup textofam fadeIn">
		En la Familia compartimos una gran pasión que nos mueve día a día, lograr las mejores uvas y criar excelentes vinos.
	</div>
	
</section>
<section id="periles">
	<div class="col-md-12" align="center">
		<h2>Dedicación Familiar Vinos con Alma</h2>
	</div>
	<div class="row">
		<div class="col-md-10 col-xs-12 col-md-offset-1 " align="center">
			<div class="borderImg">
				<img src="{{ URL::to('/') }}/images/internas/familiaAR.jpg" class="fam" alt="">
			</div>
		</div>
	</div>
	<div class="col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 textoFamilia" align="center">
		Hoy, la cuarta generación de nuestra familia ha convertido esta pasión en un estilo de vida. El cuidado personal y profesional, sumado a las características de nuestro terroir, aportan a nuestros vinos características únicas e inigualables.
	</div>
	<div class="clearfix"></div>
	<div class="perfil">
		<div id="alejandro">
			<div class="col-md-4 col-md-offset-1 col-xs-12 col-xs-offset-0" align="center">
				<div class="borderImg">
					<img src="{{ URL::to('/') }}/images/internas/AlfredoGladys.jpg" alt="">
				</div>
			</div>
			<div class="col-md-6 textoPerfil">
				<h3>Alfredo Roca y Gladys</h3>
				<p>Contar la historia de nuestra empresa es contar una historia familiar porque ambas están muy integradas. La misma la comenzamos cuando nuestros hijos nacieron, con mucho empeño y esfuerzo personal. Ahora vemos que ellos trabajan con el mismo tesón e ímpetu, tratando de lograr lo mejor para nuestra industria.
				</p>
				<p>Mi esposa y yo nos sentimos muy orgullosos y satisfechos al ver que nuestros tres hijos continúan con nuestra empresa con mucho ahinco y pasión.
				</p>
			</div>
			<div class="clearfix"></div>
			<div class="line col-md-offset-2 col-md-9 col-xs-12"></div>
			<div class="clearfix"></div>
		</div>
		<div id="alejandro">
			<div class="col-md-4 col-md-offset-1 col-xs-12 col-xs-offset-0" align="center">
				<div class="borderImg">
					<img src="{{ URL::to('/') }}/images/internas/Alejandro.jpg" alt="">
				</div>
			</div>
			<div class="col-md-6 textoPerfil">
				<h3>Alejandro Roca</h3>
				<p>Me considero un privilegiado por poder disfrutar del vino en cada momento, tanto como consumidor o hacedor, y por haber tenido una infancia entre los viñedos y bodega. Dos grandes personas como mi abuelo y mi padre me abrieron las puertas a este mundo tan lindo que es el vino y tan noble  e inmenso como es su producción.
				</p>
				<p>Actualmente estoy a cargo de la presidencia de la empresa, que es nuestra gran familia y lógicamente muy involucrado con la dirección técnica enológica y vitícola, siempre con un gran compromiso con la calidad y la búsqueda de la excelencia de nuestros vinos, alma de nuestra empresa.</p>
			</div>
			<div class="clearfix"></div>
			<div class="line col-md-offset-2 col-md-9 col-xs-12"></div>
			<div class="clearfix"></div>
		</div>
		<div id="carolina">
			<div class="col-md-4 col-md-offset-1 col-xs-12 col-xs-offset-0 " align="center">
				<div class="borderImg">
					<img src="{{ URL::to('/') }}/images/internas/Graciela.jpg" alt="Graciela Roca">
				</div>
			</div>
			<div class="col-md-6  textoPerfil">
				<h3>Graciela Roca</h3>
				<p>Mi familia siempre me transmitió mucha pasión y dedicación por el trabajo de cada día, y un gran respeto por nuestra querida tierra y sus frutos. Esto lo sentí desde chica, compartiendo con mis padres y abuelos. Es así que los ya 100 años de nuestra familia en San Rafael hacen que sienta que este terruño es gran parte de mi ADN
				</p>
				<p>En el 2003 comencé a trabajar en la bodega en el sector comercial con el objetivo permanente de llegar  a nuestros clientes y consumidores de la mejor forma posible. Es muy satisfactorio ver que  estamos creciendo gradualmente, en base a la excelencia del producto que responde acorde la mayor exigencia del consumidor.</p>
			</div>
			
			<div class="clearfix"></div>
			<div class="line col-md-offset-2 col-md-9 col-xs-12"></div>
			<div class="clearfix"></div>
		</div>
		<div id="carolina">
			<div class="col-md-4 col-md-offset-1 col-xs-12 col-xs-offset-0 " align="center">
				<div class="borderImg">
					<img src="{{ URL::to('/') }}/images/internas/Carolina.jpg" alt="">
				</div>
			</div>
			<div class="col-md-6  textoPerfil">
				<h3>Carolina Roca</h3>
				<p>Si bien siempre ha estado en relación con los viñedos y la bodega, porque esa es la actividad de mi familia, mi incorporación a la empresa ha sido reciente. Mi desempeño dentro de la misma está relacionado en todo lo referente a exportación y al equipo de administración. He tenido como guía a mi padre, hombre de mucha experiencia en este tema.
				</p>
				<p>Abrir una botella de Roca implica descubrir una pasión transmitida de generación en generación y todo el afán y dedicación que esta gran familia vuelca en cada grano de uva, en cada paso del proceso de elaboración y en la invocación constante que implementa la bodega.</p>
			</div>
			
			<div class="clearfix"></div>
			<div class=" col-md-offset-2 col-md-9 col-xs-12"></div>
			<div class="clearfix"></div>
		</div>
	</div>
	
</section>

@stop