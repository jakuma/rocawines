<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ISCC ADMIN</title>

    <!-- Bootstrap Core CSS -->
     {{ HTML::style('css/bootstrap.min.css', array('media' => 'screen')) }}
 
    <!-- Custom CSS -->
      {{ HTML::style('css/admin/sb-admin.css', array('media' => 'screen')) }}


    <!-- Morris Charts CSS -->

    <!-- Custom Fonts -->
    {{ HTML::style('css/font-awesome/css/font-awesome.min.css', array('media' => 'screen')) }}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
    
</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">ISCC Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="{{URL::to('admin')}}"><i class="fa fa-fw fa-users"></i> Cursos</a>
                    </li>
                    <li>
                        <a href="{{URL::to('admin/cat')}}"><i class="fa fa-fw fa-file-text"></i> Categorias</a>
                    </li>
  
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

       <div id="page-wrapper" style="padding-bottom:200px">
                   @yield('content')
       </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    {{ HTML::script('js/jquery.js') }}
        <!-- Bootstrap Core JavaScript -->
    
    {{ HTML::script('js/bootstrap.min.js') }}



    <!-- Morris Charts JavaScript -->
   <script languaje="JavaScript">
   var $ =jQuery;
        $(document).ready(function(){

            $(document).on("click",".borrar",function(){
                var name = $(this).attr("rel");
                var tipo =$(this).attr("tipo");
               var oka = confirm("¿Desea Elimiar  " +tipo +" : "+name+"?")
               if(!oka){
                return false;
               }
            }) 
            $(".countChar").keyup(countChar)
              function countChar() {
        var len = $(this).val().length;
        // if (len >= 500) {
        //   val.value = val.value.substring(0, 500);
        // } else {
          $('.charNum').text(len);
        // }
      }; 


       $('#foto').on('change', function(event) {
 // we fetch the file data
 var image = this.files[0]; // not $(this)
 
 //now, we need to empty the preview div because if not, the image selected will append to what was there alread.
 
 $('#preview').html(''); //we set the innerHTML of the div to null
 
 var reader = new FileReader(); // the jQuery FileReader class
 reader.onload = function(e){ // whatever we want FileReader to do, wee need to do that when it loads
 $('<img src="' + e.target.result + '" class="" style="width:100%;height:auto" alt="Loading..">').appendTo($('#preview'));
 // What we just did: 
 // we fetched the base64 data of the image (e.target.result) 
 // and assigned to a new img element's src attribute
 // then appended it to the preview div.
 // for more info on FileReader() and jQuery's appendTo() please check the links in the articla.
 }
 reader.readAsDataURL(image); // this gives our file to the FileReader() and uses the onload function to do our bidding.
 });
        })
   </script>
</body>

</html>
