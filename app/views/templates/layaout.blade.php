<?php if(isset($lang)){$lang="es";} ?>
<!DOCTYPE html>
<html lang="{{$lang}}">
  <head>
    <?php if(isset($title)){?>
       <title>{{$title}}</title>
    <?php } else {?>
    <title>Bodega Alfredo Roca, San Rafael , Mendoza, Argentina</title>
     <?php } ?>
    <?php if(isset($descripcion)){?>
      <meta name="description" content="{{$descripcion}}">
    <?php } ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="{{URL::to('/')}}/images/favicon.ico" /> 

    <link rel='stylesheet' id='google-fonts-css'  href='http://fonts.googleapis.com/css?family=Libre+Baskerville%3A400%2C700%2C400italic&#038;ver=4.4.1' type='text/css' media='all' />
    {{ HTML::style('css/bootstrap.min.css', array('media' => 'screen')) }}
    {{ HTML::style('css/settings.css', array('media' => 'screen')) }}
    {{ HTML::style('css/styles.css', array('media' => 'screen')) }}
    {{ HTML::style('css/font-awesome/css/font-awesome.min.css', array('media' => 'screen')) }}
    @if(isset($css_array))
    @foreach($css_array as $var)
    {{ HTML::style($var, array('media' => 'screen')) }}
    @endforeach
    @endif
    @if(isset($css_print_array))
    @foreach($css_print_array as $var)
    {{ HTML::style($var, array('media' => 'print')) }}
    @endforeach
    @endif
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5shiv.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <![endif]-->
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCbEJ826CbvSGwFTtO-KTWoowpdn3GxF50&sensor=false"></script>
      <!--  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  -->


  </head>
  <body class="full-width">
  
<div class="container">
  <nav class="navbar navbar-roca">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" id="menuleft" class="navbar-toggle " data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <i class="fa fa-bars fa-2x"></i>
          </button>
      
      
        
      </div>

      <div class="brand-centered" align="center">
      <a class="navbar-brand" href="{{URL::to($lang.'/')}}"><img style="margin-right: 10px; padding: 0;width:auto; height:55px; " src="{{ URL::to('/') }}/images/logoRoca.png" alt="Dispute Bills">
      </a>
      </div>
      
      <div id="navRoca" class="">
        <ul class="nav navbar-nav navbar-left">
          <li class="active"><a href="{{URL::to($lang.'/')}}/familia">FAMILIA</a></li>
          <li><a href="{{URL::to($lang.'/')}}/bodega">BODEGA</a></li>
           <li><a href="{{URL::to($lang.'/')}}/vinos">VINOS</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="{{URL::to($lang.'/')}}/vinedos">VIÑEDOS</a></li>
          <li class="active"><a href="{{URL::to($lang.'/')}}/visitas">VISITAS</a></li>
           <li class="active"><a href="{{URL::to($lang.'/')}}/contacto">CONTACTO</a></li>

        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
  </nav>
</div>
  <!--sidebar end-->
      <!--Inicio contenedor Principal-->
      <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
          @yield('content')
        </main>
      </div>
      <!--Contenedor ppal end-->
<footer>
  <div class="redes">
    <div class="col-md-6 col-xs-12 beber">
        Beber con moderación. Prohibida su venta a menores de 18 años.
    </div>
    <div class="col-md-2 col-xs-6" align="center">
        <a target="_blank" href="http://www.wineinmoderation.eu/">
         <img src="{{URL::to('/')}}/images/wim.png" alt="">
        </a>
    </div>
    <div class="col-md-4 col-xs-6" align="right">
      <a target="_blank" href="http://twitter.com/AlfredoRoca" style="margin-left:20px">
        <i class="fa fa-twitter fa-2x"></i>
      </a>
      <a target="_blank" href="http://www.instagram.com/alfredo_roca_bodegayvinedos" style="margin-left:20px">
        <i class="fa fa-instagram fa-2x"></i>
       
      </a>
      <a target="_blank" href="http://www.facebook.com/BodegaAlfredoRoca" style="margin-left:20px">
        <i class="fa fa-facebook-official fa-2x"></i>
       
      </a>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="col-md-4 datos-footer" align="center">
    <img src="{{ URL::to('/') }}/images/logo_blanco_vertical.png" alt="">
  </div>
  <div class="col-md-2 hidden-xs datos-footer borderLeft">
    <div class="footer-titulo">Mapa Sitio
   </div>
    <ul>
      <li><a href="{{URL::to('/familia')}}">Familia</a></li>
      <li><a href="{{URL::to('/bodega')}}">Bodega</a></li>
      <li><a href="{{URL::to('/vinos')}}">Vinos</a></li>
      <li><a href="{{URL::to('/vinedos')}}">Viñedos</a></li>
      <li><a href="{{URL::to('/turismo')}}">Turismo</a></li>
    </ul>
  </div>
  <div class="col-md-2 hidden-xs datos-footer">
    <div class="footer-titulo">Donde Estamos
   </div>
   <div class="texto">
     Ruta Provincial 165 esq. <br>calle La Pichana 
M5603XAG – <br>Cañada Seca, San Rafael, Mendoza, 
Argentina
<br>
<b>GPS:</b> 34° 40.574' S - 68° 14.529' O

   </div>
  </div>
  <div class="col-md-3 col-xs-12 datos-footer">
     <div class="footer-titulo">Nuestros Teléfonos
   </div>

   <div class="texto">
   <b>  +  54  260  4497250  /   4497117  /  4497194</b>
<br>(de lunes  a viernes  hábiles  de 7:30  a 16:30 hs)

   </div>
  <!-- <div class="input-group">
      <input type="text" class="form-control" placeholder="su email ..">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">Enviar!</button>
      </span>
    </div> -->
  </div>
  <div class="clearfix"></div>
  <div class="fondo">
    <div class="col-md-8" align="left">
      Terminos y condiciones | Politicas de Privacidad | Copyright ROCA S.A. © 2016
    </div>
    <div class="col-md-4 design" align="right"> Design by : <a href="http://dbgcreative.com.ar" class="dbg">DBG Creative</a></div>
    <div class="clearfix"></div>
  </div>
</footer>
      <input type="hidden" value="{{ URL::to('/') }}" id="base_url">
         <input type="hidden" value="{{$page}}" id="page">
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <!-- // ]<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script> -->
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      {{ HTML::script('js/jquery.js') }}
      {{ HTML::script('js/bootstrap.min.js') }}
      {{ HTML::script('js/jquery.scrollify.min.js') }}
      {{ HTML::script('js/gral_scripts.js') }}

      
      @if(isset($js_array))
      @foreach($js_array as $var)
      {{ HTML::script($var) }}
      @endforeach;
      @endif
      
      <script>
      (function($){
      $(document).ready(function() {
      // $('#tweecool').tweecool({
      // // Your twitter username
      // username : 'jakuma_',
      
      // // Number of tweets to show
      // limit : 3,
      
      
      // // Show profile image
      // profile_image : false,
      
      
      // // Show tweet time
      // show_time : true,
      
      // // Show media
      // show_media : false,
      
      // show_media_size: '2'
      // //values: small, large, thumb, medium
      // });
      $('a[href*=#]').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      && location.hostname == this.hostname) {
      var $target = $(this.hash);
      $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
      if ($target.length) {
      var targetOffset = $target.offset().top;
      $('html,body').animate({scrollTop: targetOffset}, 1000);
      return false;
      }
      }
      });
      });
      })(jQuery);
      </script>
     
    </body>
  </html>