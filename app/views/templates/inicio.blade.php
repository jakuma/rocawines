<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Bodega Alfredo Roca, San Rafael , Mendoza, Argentina</title>
     <meta name="description" content="Bienvenidos a Bodega Roca en San Rafael. Los invitamos a conocer nuestra bodega, nuestros viñedos y la elaboracion de nuestros vinos. Descubra la pasión con que creamos nuestros vinos... ">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link rel='stylesheet' id='google-fonts-css'  href='http://fonts.googleapis.com/css?family=Libre+Baskerville%3A400%2C700%2C400italic&#038;ver=4.4.1' type='text/css' media='all' />
    {{ HTML::style('css/bootstrap.min.css', array('media' => 'screen')) }}
    {{ HTML::style('css/settings.css', array('media' => 'screen')) }}
    {{ HTML::style('css/styles.css', array('media' => 'screen')) }}
    {{ HTML::style('css/font-awesome/css/font-awesome.min.css', array('media' => 'screen')) }}
  

  </head>
  <body class="full-width">
    @yield('content')

      {{ HTML::script('js/jquery.js') }}
      {{ HTML::script('js/bootstrap.min.js') }}
      {{ HTML::script('js/jquery.scrollify.min.js') }}
      {{ HTML::script('js/inicio.js') }}
      
      
      
    </body>
  </html>